//
//  CustomTableViewCell.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 02/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
