//
//  Constant.h
//  SuperAdmin
//
//  Created by Gauri Shankar on 02/10/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4S (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

///API

#define KBaseUrl @"http://bnotifi.com/super/media/projects/"

#define KPostProjectUrl @"http://bnotifi.com/super/rest/post_project"
#define KLoginUrl @"http://bnotifi.com/super/rest/login"
#define KForgotPasswordUrl @"http://bnotifi.com/super/rest/forgotpassword"
#define KSignupUrl @"http://bnotifi.com/super/rest/signup"
#define KAcitiveNotifiUrl @"http://bnotifi.com/super/rest/get_acitive_notification_count"
#define KMusicUrl @"http://bnotifi.com/super/rest/get_music"
#define KProductsUrl @"http://bnotifi.com/super/rest/get_products"
#define KVideosUrl @"http://bnotifi.com/super/rest/get_videos"

#define KGalleryUrl @"http://bnotifi.com/super/rest/get_all_notification"

#define KDeleteReceivedNotifiUrl @"bnotifi.com/super/rest/delete_received_notification"
//user_id/user_media_id
#define KReceivedNotificationUrl @"http://bnotifi.com/super/rest/get_received_notification"

#define KInteractiveButtonUrl @"http://bnotifi.com/super/rest/media_button"

#define KInteractiveNewButtonUrl @"http://bnotifi.com/super/rest/get_media_details"

///Old
//API Urls

#define KBaseServicesUrl @"http://bnotifi.com/super/rest/get_services"
#define KBaseChangePasswordUrl @"http://bnotifi.com/super/rest/change_password"
#define KBaseDownlineUrl @"http://bnotifi.com/super/rest/get_downline"

#define KBaseAllNotificationUrl @"http://bnotifi.com/super/rest/get_all_notification"
#define KBaseReceivedNotificationUrl @"http://bnotifi.com/super/rest/get_received_notification"
#define KBaseSentNotificationUrl @"http://bnotifi.com/super/rest/get_sent_notification"
#define KBaseActiveNotificationUrl @"http://bnotifi.com/super/rest/get_acitve_notification_count"

#define KBaseInsertMediaViewUrl @"http://bnotifi.com/super/rest/insert_media_view"

#define KBaseDeleteSentNotificationUrl @"http://bnotifi.com/super/rest/delete_sent_notification"

#define KBaseDeleteReceivedNotificationUrl @"http://bnotifi.com/super/rest/delete_received_notification"

#define KBaseSendNotificationUrl @"http://bnotifi.com/super/rest/send_user_notification"

//Notification Url
//#define KBaseViewUrl @"http://bnotifi.com/super/rest/update_media_view"


#define KBasePaymentUrl @"http://bnotifi.com/super/rest/upgrade"

#define KBaseCustomerBecomeCoach @"http://bnotifi.com/super/rest/becomeacoach"

#define KBaseMediaUrl @"http://bnotifi.com/super/media/media_type"

#define KUserId @"UserId"

#define APP_NAME    @"Super Admin"
#define ALERT_OK    @"OK"

#endif /* Constant_h */
