//
//  DetailViewController.h
//  NotificationPOC
//
//  Created by RnF-12 on 15/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
{
    NSDictionary *dictData;
}

@property (strong, nonatomic)    NSDictionary *dictData;

@end
