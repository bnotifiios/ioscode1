//
//  AppDelegate.h
//  SuperAdmin
//
//  Created by Gauri Shankar on 02/10/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

{
    UINavigationController *navController;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSDictionary *dictProject;

+ (AppDelegate *)sharedAppDelegate;
+ (UIStoryboard *)storyBoardType;
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end

