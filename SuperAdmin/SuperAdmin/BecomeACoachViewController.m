//
//  BecomeACoachViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 30/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "BecomeACoachViewController.h"
#import "Constant.h"
#import "MainCoachViewController.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import "UIImageView+WebCache.h"


@interface BecomeACoachViewController ()<UITextFieldDelegate>
{
    IBOutlet UITextField *txtID;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    IBOutlet UILabel *lblTitle,*lblDSC;
}
@end

@implementation BecomeACoachViewController

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
    
    //After entering your Coach ID you will not only get the benefits previously described but you will also have the ability to Upgrade your account so that you can actually SEND great content to your friend and team members.
    
    lblTitle.text = _topTitle;
    lblDSC.text = [NSString stringWithFormat:@"After entering your %@ ID you will not only get the benefits previously described but you will also have the ability to Upgrade your account so that you can actually SEND great content to your friend and team members.",[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"coach_text"]];
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];


}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)submitAction:(id)sender
{
    if([txtID.text length] ==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*ID Number is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
else
{
    [self CallAPI];
}

}

#pragma mark API Methods

-(void)CallAPI
{
    NSLog(@"CallAPI");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseCustomerBecomeCoach]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];

    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"id_number"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtID.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //project_id
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         NSDictionary *dictTemp = [[response objectForKey:@"data"] objectAtIndex:0];
                         
                         //[[NSUserDefaults standardUserDefaults] setObject:dictTemp forKey:@"Response"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"id"] forKey:@"user_id"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"email"] forKey:@"email"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_coach"] forKey:@"is_coach"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_paid"] forKey:@"is_paid"];
                         
                         if([[dictTemp objectForKey:@"is_coach"] integerValue] == 0)
                         {
                             [self.navigationController popViewControllerAnimated:YES];
                         }
                         else
                         {
                             MainCoachViewController *objSearchView = (MainCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainCoachViewControllerId"];
                             
                             [self.navigationController pushViewController:objSearchView animated:YES];
                             objSearchView = nil;
                         }

                         

                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    [uploadTask resume];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 10)
    {
    if(buttonIndex == 0)
    {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
