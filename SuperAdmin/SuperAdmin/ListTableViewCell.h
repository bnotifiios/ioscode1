//
//  ListTableViewCell.h
//  NotificationPOC
//
//  Created by Gauri Shankar on 15/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewCell : UITableViewCell

@property (weak, nonatomic)     IBOutlet UILabel *lblTitle,*lblDate;
@property (weak, nonatomic)     IBOutlet UIImageView *imgProduct,*imgView;
@property (weak, nonatomic)     IBOutlet UIButton *btnDelete,*btnView,*btnShare,*btnSend;
@end
