//
//  SignUpViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 30/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "SignUpViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "MainViewController.h"

@interface SignUpViewController ()<UITextFieldDelegate>
{
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    IBOutlet UITextField *txtFN,*txtLN,*txtEmail,*txtPwd,*txtCwd;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;

}

@end

@implementation SignUpViewController

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}


-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [scrollView contentSizeToFit];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView1 willDecelerate:(BOOL)decelerate
{
    CGPoint offset = scrollView1.contentOffset;
    [scrollView1 setContentOffset:offset animated:NO];
    
}

-(IBAction)signUpAction:(id)sender
{
    NSLog(@"signUpAction");
    
        if([txtFN.text length] ==0)
        {

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*First Name is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
   else if([txtLN.text length] ==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Last Name is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }

        else if([txtEmail.text length] ==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Email Address is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
        else if(![self validateEmailWithString:txtEmail.text])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter a valid email address." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            return;
        }
        else if([txtPwd.text isEqualToString:@""])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password field can't be empty" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            return;
            
        }
        else if ([txtCwd.text isEqualToString:@""])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Password field can't be empty" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            return;
            
        }
        else if (![txtPwd.text isEqualToString:txtCwd.text])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your passwords do not match" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            return;
            
        }
        else if([txtPwd.text length] < 6)
        {
            UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"oops!" message:@"Your password should be atleast 6 characters" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
        else
        {
            [self CallSignUpAPI];
            
        }
    
}

#pragma mark API Methods

-(void)CallSignUpAPI
{
    NSLog(@"CallSignUpAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KSignupUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"first_name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtFN.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"last_name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtLN.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtEmail.text] dataUsingEncoding:NSUTF8StringEncoding]];

    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"password"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtPwd.text] dataUsingEncoding:NSUTF8StringEncoding]];

    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"udid"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"src"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"iOS\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"token"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         /*
                          {
                          data =     (
                          {
                          activation = "<null>";
                          email = "tap@gmail.com";
                          "first_name" = Tap;
                          id = 313;
                          "id_number" = "";
                          "is_coach" = 0;
                          "is_delete" = 0;
                          "is_paid" = 0;
                          "last_name" = Tap;
                          lastvisit = 0;
                          online = 0;
                          password = 1ffaec673df7c8d03e7931d91cbda1bb787ecee9;
                          phone = "";
                          "project_id" = 11;
                          "ref_number" = "";
                          registered = "2016-11-02";
                          src = "";
                          status = active;
                          token = "";
                          udid = "";
                          username = "";
                          }
                          );
                          resultText = Success;
                          status = success;
                          }
                         
   */
                        
                        
                         [[NSUserDefaults standardUserDefaults] setObject:[[[response objectForKey:@"data"] objectAtIndex:0] valueForKey:@"id"] forKey:@"user_id"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:txtEmail.text forKey:@"email"];
                         
                         
                             MainViewController *objSearchView = (MainViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainViewControllerId"];
                             
                             [self.navigationController pushViewController:objSearchView animated:YES];
                             objSearchView = nil;
                         
                        
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtFN) {
        [txtFN resignFirstResponder];
        [txtLN becomeFirstResponder];
    }
    else if (textField == txtLN)
    {
        [txtLN resignFirstResponder];
        [txtEmail becomeFirstResponder];
    }
    else if (textField == txtEmail)
    {
        [txtEmail resignFirstResponder];
        [txtPwd becomeFirstResponder];
    }
    else if (textField == txtPwd)
    {
        [txtPwd resignFirstResponder];
        [txtCwd becomeFirstResponder];
    }
    else if (textField == txtCwd)
    {
        [textField resignFirstResponder];

    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
