//
//  HomeViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 30/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "HomeViewController.h"
#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "SignUpCoachViewController.h"
#import "SignUpCustomerViewController.h"

@interface HomeViewController ()
{
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
}
@end

@implementation HomeViewController

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
    
    //NSLog(@"[AppDelegate sharedAppDelegate].dictProject %@",[AppDelegate sharedAppDelegate].dictProject);
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];
}

-(IBAction)loginAction:(id)sender
{
    NSLog(@"loginAction");
    
    LoginViewController *objSearchView = (LoginViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"LoginViewControllerId"];
    
    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;

}

-(IBAction)signUpAction:(id)sender
{
    NSLog(@"signUpAction");
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_mlm"] isEqualToString:@"1"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ACCOUNT TYPE" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"i_am_customer_text"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"i_am_coach_text"],@"Cancel", nil];
        [alert show];
    }
    else
    {
        SignUpViewController *objSearchView = (SignUpViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"SignUpViewControllerId"];
    
        [self.navigationController pushViewController:objSearchView animated:YES];
        objSearchView = nil;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        SignUpCustomerViewController *objSearchView = (SignUpCustomerViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"SignUpCustomerViewControllerId"];
        
        [self.navigationController pushViewController:objSearchView animated:YES];
        objSearchView = nil;
        
    }
    else if (buttonIndex == 1) {
        
        SignUpCoachViewController *objSearchView = (SignUpCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"SignUpCoachViewControllerId"];
        
        [self.navigationController pushViewController:objSearchView animated:YES];
        objSearchView = nil;
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
