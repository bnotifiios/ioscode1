//
//  PlayViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 09/07/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "PlayViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "Constant.h"

@interface PlayViewController ()
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;


}
@end

@implementation PlayViewController

@synthesize strUrl,strTitle;

- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self VideoPlayButtonAction];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
    
    lblTitle.text = strTitle;
    
}

#pragma mark Video Play

-(void)VideoPlayButtonAction
{
    NSLog(@"VideoPlayButtonAction");
    
    if ([strUrl length]>0)
    {
        NSString *urlString =
        [NSString stringWithFormat:@"%@",
         [strUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        
        // asset is a video
        AVPlayer *avPlayer = [[AVPlayer alloc] initWithURL:[NSURL URLWithString:urlString]];
        //avPlayer.muted = YES;
        
        NSLog(@"Video url %@",urlString);
        
        AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avPlayer];
        UIView *subView;
        
        
        if(IS_IPHONE_5)
        {
            [avPlayerLayer setFrame:CGRectMake(0, -5, 320, 568)];
            subView = [[UIView alloc]initWithFrame:CGRectMake(0, 70, 320, 498)];
        }
        else if(IS_IPHONE_6)
        {
            [avPlayerLayer setFrame:CGRectMake(0, -5, 375, 667)];
            subView = [[UIView alloc]initWithFrame:CGRectMake(0, 70, 375, 597)];
            
        }
        else if(IS_IPHONE_6P)
        {
            [avPlayerLayer setFrame:CGRectMake(0, -5, 414, 768)];
            subView = [[UIView alloc]initWithFrame:CGRectMake(0, 70, 414, 666)];
            
        }
        else
        {
            [avPlayerLayer setFrame:CGRectMake(0, -5, 320, 568)];
            subView = [[UIView alloc]initWithFrame:CGRectMake(0, 70, 320, 498)];
            
        }
        //avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
 
        subView.tag = 11;
        [subView.layer addSublayer:avPlayerLayer];
        
        [self.view addSubview:subView];
        
        [avPlayer play];
        
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[avPlayer currentItem]];
        
    }
    else
    {
        NSLog(@"Video not available");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video not available" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    
    for (UIView *i in self.view.subviews)
    {
        if(i.tag == 11){
            
            NSLog(@"playerItemDidReachEnd");
            
            //[i removeFromSuperview];
            break;
            //i = nil;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
