//
//  LoginViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 30/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "LoginViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "SignUpViewController.h"
#import "AppDelegate.h"
#import "MainCoachViewController.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "ForgotPwdViewController.h"
#import "HomeViewController.h"
#import "MainViewController.h"
#import "NewCustomerViewController.h"
#import "SignUpCoachViewController.h"
#import "SignUpCustomerViewController.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    IBOutlet UITextField *txtEmail,*txtPwd;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;

}


@end

@implementation LoginViewController

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    
    
    
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [scrollView contentSizeToFit];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];

}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView1 willDecelerate:(BOOL)decelerate
{
    CGPoint offset = scrollView1.contentOffset;
    [scrollView1 setContentOffset:offset animated:NO];
    
}

-(IBAction)backAction:(id)sender
{
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"] isEqualToString:@""])
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"status"] isEqualToString:@"no"])
        {
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else
        {
            //[[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"status"];//KUserId
            
            HomeViewController *objSearchView = (HomeViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"HomeViewControllerId"];
            
            [self.navigationController pushViewController:objSearchView animated:YES];
            objSearchView = nil;
        }
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];

    }
}


-(IBAction)loginAction:(id)sender
{
    NSLog(@"loginAction");
    
    if([txtEmail.text length] ==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Address is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if (![self validateEmailWithString:txtEmail.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter valid email address!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if([txtPwd.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        [self CallLoginAPI];

    }
}


- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(IBAction)forgotPwdAction:(id)sender
{
    ForgotPwdViewController *objSearchView = (ForgotPwdViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ForgotPwdViewControllerId"];
    
    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
  
}

-(IBAction)signUpAction:(id)sender
{
    NSLog(@"signUpAction");

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_mlm"] isEqualToString:@"1"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ACCOUNT TYPE" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"i_am_customer_text"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"i_am_coach_text"],@"Cancel", nil];
        alert.tag = 10;
        [alert show];
    }
    else
    {
        SignUpViewController *objSearchView = (SignUpViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"SignUpViewControllerId"];
        
        [self.navigationController pushViewController:objSearchView animated:YES];
        objSearchView = nil;
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 10)
    {
    
        if (buttonIndex == 0)
        {
            SignUpCustomerViewController *objSearchView = (SignUpCustomerViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"SignUpCustomerViewControllerId"];
            
            [self.navigationController pushViewController:objSearchView animated:YES];
            objSearchView = nil;
            
        }
        else if (buttonIndex == 1) {
            
            SignUpCoachViewController *objSearchView = (SignUpCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"SignUpCoachViewControllerId"];
            
            [self.navigationController pushViewController:objSearchView animated:YES];
            objSearchView = nil;
            
        }
    }
}

#pragma mark API Methods

-(void)CallLoginAPI
{
    NSLog(@"CallLoginAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KLoginUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtEmail.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"password"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtPwd.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
            // NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         /*
                          
                          {
                          data =     (
                          {
                          activation = "<null>";
                          email = "test@gmail.com";
                          "first_name" = Test;
                          id = 182;
                          "id_number" = "";
                          "is_coach" = 0;
                          "is_delete" = 0;
                          "is_paid" = 0;
                          "last_name" = Test;
                          lastvisit = 0;
                          online = 0;
                          password = 1ffaec673df7c8d03e7931d91cbda1bb787ecee9;
                          phone = "";
                          "project_id" = 39;
                          "ref_number" = "";
                          registered = "2016-09-24";
                          src = "";
                          status = active;
                          token = "";
                          udid = "";
                          username = "";
                          }
                          );
                          resultText = Success;
                          status = success;
                          }
                        
                          */

                         [[NSUserDefaults standardUserDefaults] setObject:[[[response objectForKey:@"data"] objectAtIndex:0] valueForKey:@"id"] forKey:@"user_id"];

                         [[NSUserDefaults standardUserDefaults] setObject:txtEmail.text forKey:@"email"];

                         if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_mlm"] isEqualToString:@"0"])
                         {
                             MainViewController *objSearchView = (MainViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainViewControllerId"];
                             
                             [self.navigationController pushViewController:objSearchView animated:YES];
                             objSearchView = nil;
                             
                         }
                         else
                         {
                             NSDictionary *dictTemp = [[response objectForKey:@"data"] objectAtIndex:0];
                             
                             //[[NSUserDefaults standardUserDefaults] setObject:dictTemp forKey:@"Response"];
                             
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"id_number"] forKey:@"id_number"];
                             
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"id"] forKey:@"user_id"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"email"] forKey:@"email"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_coach"] forKey:@"is_coach"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_paid"] forKey:@"is_paid"];
                             
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"first_name"] forKey:@"first_name"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"last_name"] forKey:@"last_name"];
                             
                             if([[dictTemp objectForKey:@"is_coach"] integerValue] == 0)
                             {
                                 NewCustomerViewController *objSearchView = (NewCustomerViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"NewCustomerViewControllerId"];
                                 
                                 [self.navigationController pushViewController:objSearchView animated:YES];
                                 objSearchView = nil;
                                 
                                 
                             }
                             else
                             {
                                 
                                 
                                 MainCoachViewController *objSearchView = (MainCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainCoachViewControllerId"];
                                     
                                     [self.navigationController pushViewController:objSearchView animated:YES];
                                     objSearchView = nil;

                                 
                             }
                             
                             
                         }

                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtEmail)
    {
        [txtEmail resignFirstResponder];
        [txtPwd becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];

    }
    
    //[textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
