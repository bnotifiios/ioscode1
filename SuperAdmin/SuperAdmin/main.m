//
//  main.m
//  SuperAdmin
//
//  Created by Gauri Shankar on 02/10/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
