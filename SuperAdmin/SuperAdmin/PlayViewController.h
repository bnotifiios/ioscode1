//
//  PlayViewController.h
//  NotificationPOC
//
//  Created by Gauri Shankar on 09/07/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayViewController : UIViewController

@property (strong, nonatomic) NSString *strUrl,*strTitle;

@end
