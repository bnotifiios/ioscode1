//
//  ProjectCodeVC.m
//  SuperAdmin
//
//  Created by Gauri Shankar on 22/10/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import "ProjectCodeVC.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"

@interface ProjectCodeVC ()<UITextFieldDelegate>
{
    IBOutlet UITextField *txtProjectId;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;

}
@end

@implementation ProjectCodeVC

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}


-(IBAction)submitAction:(id)sender
{
    NSLog(@"submitAction");
    
    if([txtProjectId.text length] ==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Project Id is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else
    {
        [self CallProjectIdAPI];
        
    }
}


#pragma mark API Methods

-(void)CallProjectIdAPI
{
    NSLog(@"CallProjectIdAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KPostProjectUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_code"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtProjectId.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"Processing....", nil);
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                        /*
                         //wiz,mlm==1
                         {
                         data =     (
                         {
                         "background_color" = "#6FE2FF";
                         "background_color2" = "#7ECAFF";
                         "background_color3" = "#6FE2FF";
                         "background_image" = "";
                         "be_a_coach" = "Become A Customer";
                         "changepassword_text" = "change password";
                         "coach_text" = "";
                         "created_date" = "0000-00-00";
                         "customer_text" = "My Downline";
                         "gallery_text" = Gallery;
                         "header_color" = 7F75FF;
                         "home_intro" = "";
                         "i_am_coach" = "I Am A Coach";
                         "i_am_customer" = "I Am A Customer";
                         id = 39;
                         "is_active" = 1;
                         "is_mlm" = 1;
                         "modify_permission" = 0;
                         "music_text" = 0;
                         "my_downline_text" = "My Downline";
                         "notification_text" = Messages;
                         password = 5543a2c29e2ba6ed629494518f0b1e643f0cde6a;
                         "product_music_type" = 0;
                         "products_text" = Products;
                         "project_code" = wiz;
                         "project_email" = "wbckevin@me.com";
                         "project_logo" = "57e19e467b614_Wiz_Brain_Tech_PNG.png";
                         "project_name" = wiz;
                         "project_phone" = wiz;
                         "project_theme" = "style_green";
                         "received_text" = Received;
                         "select_type" = 0;
                         "sent_text" = Sent;
                         "share_the_business" = "Share The Business";
                         transparency = "0.1";
                         "upgrade_text" = "Upgrade Your Membership";
                         username = wiz;
                         "video_text" = Videos;
                         }
                         );
                         datasource = "http://bnotifi.com/super/media/projects/";
                         resultText = Success;
                         status = success;
                         }

                         //
                         {
                         data =     (
                         {
                         "background_color" = "#6FE2FF";
                         "background_color2" = "#7ECAFF";
                         "background_color3" = "#6FE2FF";
                         "background_image" = "";
                         "be_a_coach" = "Become A Customer";
                         "changepassword_text" = "change password";
                         "coach_text" = "";
                         "created_date" = "0000-00-00";
                         "customer_text" = "";
                         "gallery_text" = Gallery;
                         "home_intro" = "";
                         "i_am_coach" = "I Am A Coach";
                         "i_am_customer" = "I Am A Customer";
                         id = 39;
                         "is_active" = 1;
                         "is_mlm" = 1;
                         "modify_permission" = 0;
                         "music_text" = 0;
                         "my_downline_text" = "My Downline";
                         "notification_text" = Messages;
                         password = 5543a2c29e2ba6ed629494518f0b1e643f0cde6a;
                         "product_music_type" = 0;
                         "products_text" = Products;
                         "project_code" = wiz;
                         "project_email" = "wbckevin@me.com";
                         "project_logo" = "57e19e467b614_Wiz_Brain_Tech_PNG.png";
                         "project_name" = WizBrainTech;
                         "project_phone" = 2147483647;
                         "project_theme" = "style_green";
                         "received_text" = Received;
                         "select_type" = 0;
                         "sent_text" = Sent;
                         "share_the_business" = "Share The Business";
                         transparency = "0.6";
                         "upgrade_text" = "Upgrade Your Membership";
                         username = wiz;
                         "video_text" = Videos;
                         }
                         );
                         datasource = "http://bnotifi.com/super/media/projects/";
                         resultText = Success;
                         status = success;
                         }

                         */
                         
                         //is_mlm
                         [[NSUserDefaults standardUserDefaults] setObject:txtProjectId.text forKey:@"Application_Key"];

                         [[NSUserDefaults standardUserDefaults] setObject:[[[response objectForKey:@"data"] objectAtIndex:0] valueForKey:@"is_mlm"] forKey:@"is_mlm"];

                         [[NSUserDefaults standardUserDefaults] setObject:[[[response objectForKey:@"data"] objectAtIndex:0] valueForKey:@"id"] forKey:@"project_id"];


                         [[NSUserDefaults standardUserDefaults] setObject:[[[response objectForKey:@"data"] objectAtIndex:0] valueForKey:@"product_music_type"]forKey:@"product_music_type"];

                         [AppDelegate sharedAppDelegate].dictProject = [[response objectForKey:@"data"] objectAtIndex:0];
                         [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"data"] objectAtIndex:0] forKey:@"project_detail"];

//                         [imgViewAppLogo
//                          sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"]valueForKey:@"project_logo"]]]
//                          placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
                         
//                         [imgViewBg
//                          sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[AppDelegate sharedAppDelegate].dictProject valueForKey:@"background_image"]]]
//                          placeholderImage:[UIImage imageNamed:@""]];
                         
                         //[self.view setBackgroundColor:[AppDelegate colorFromHexString:[[AppDelegate sharedAppDelegate].dictProject valueForKey:@"background_color"]]];
                         
                         HomeViewController *objSearchView = (HomeViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"HomeViewControllerId"];
                         
                         [self.navigationController pushViewController:objSearchView animated:YES];
                         objSearchView = nil;
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
    [uploadTask resume];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
