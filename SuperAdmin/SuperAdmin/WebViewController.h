//
//  WebViewController.h
//  NotificationPOC
//
//  Created by Gauri Shankar on 02/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
{
    NSDictionary *dictData;
}

@property (strong, nonatomic)    NSDictionary *dictData;

@property (strong, nonatomic) NSString *strUrl,*strTitle,*strMediaType;

@end
