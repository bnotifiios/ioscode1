//
//  ShareViewController.h
//  NotificationPOC
//
//  Created by Gauri Shankar on 14/05/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareViewController : UIViewController
{
    NSString *dictData;
}

@property (strong, nonatomic)    NSString *dictData;

@end
