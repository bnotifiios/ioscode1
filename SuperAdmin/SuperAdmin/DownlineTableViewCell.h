//
//  DownlineTableViewCell.h
//  NotificationPOC
//
//  Created by Gauri Shankar on 04/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownlineTableViewCell : UITableViewCell
@property(weak,nonatomic) IBOutlet UILabel *lblName,*lblType,*lblStatus;

@end
