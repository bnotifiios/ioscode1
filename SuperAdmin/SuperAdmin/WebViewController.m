//
//  WebViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 02/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "WebViewController.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "AppDelegate.h"

@interface WebViewController ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *webViewUrl;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
}
@end

@implementation WebViewController

@synthesize strUrl,strTitle,strMediaType;
@synthesize dictData;

- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
    
    lblTitle.text = strTitle;
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[AppDelegate sharedAppDelegate].dictProject valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[AppDelegate sharedAppDelegate].dictProject valueForKey:@"background_image"]]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[AppDelegate sharedAppDelegate].dictProject valueForKey:@"background_color"]]];

    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webViewUrl setScalesPageToFit:YES];
    [webViewUrl loadRequest:request];
    
    [self submitView];
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //[MBProgressHUD hideHUDForView:self.view animated:YES];

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)submitView
{
    NSLog(@"submitView");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseInsertMediaViewUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    //user_id, user_media_id
    
    //user_id
    //    user_media_id
    //    media_id
    //    project_id
    //    src
    //view_date
    
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_view_type"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",strMediaType] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([dictData objectForKey:@"media_id"])
    {
        [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"src"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"ios\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    if ([dictData objectForKey:@"user_media_id"])
    {
        // contains object
        [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"user_media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];

    }
    else
    {
        [body appendData:[[NSString stringWithFormat:@"0\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         
                     }
                     else
                     {
                         //                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         //                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     //                     [alert show];
                 }
             }
             
         }
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
