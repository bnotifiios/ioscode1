//
//  SignUpCoachViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 30/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "SignUpCoachViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "MainCoachViewController.h"
#import "MainViewController.h"
#import "UIImageView+WebCache.h"

@interface SignUpCoachViewController ()<UITextFieldDelegate>
{
    IBOutlet TPKeyboardAvoidingScrollView *scrollView;
     IBOutlet UITextField *txtFN,*txtLN,*txtRcode,*txtEmail,*txtPwd,*txtCwd,*txtIdNo;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;

}

@end

@implementation SignUpCoachViewController


- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [scrollView contentSizeToFit];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView1 willDecelerate:(BOOL)decelerate
{
    CGPoint offset = scrollView1.contentOffset;
    [scrollView1 setContentOffset:offset animated:NO];
    
    
}

-(IBAction)signUpAction:(id)sender
{
    NSLog(@"signUpAction");
    
    if([txtFN.text length] ==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*First Name is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if([txtLN.text length] ==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Last Name is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if([txtEmail.text length] ==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Email Address is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if(![self validateEmailWithString:txtEmail.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter a valid email address." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    else if([txtPwd.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password field can't be empty" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
        
    }
    else if ([txtCwd.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Password field can't be empty" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
        
    }
    else if (![txtPwd.text isEqualToString:txtCwd.text])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your passwords do not match" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
        
    }
    else if([txtPwd.text length] < 6)
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"oops!" message:@"Your password should be atleast 6 characters" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if([txtRcode.text length] ==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Referrer ID is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else if([txtIdNo.text length] ==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"*Id Number is required." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    else
    {
        [self CallSignUpAPI];
        
    }
    
}

#pragma mark API Methods

-(void)CallSignUpAPI
{
    NSLog(@"CallSignUpAPI");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KSignupUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"first_name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtFN.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"last_name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtLN.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtEmail.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"password"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtPwd.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"ref_number"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtRcode.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"id_number"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",txtIdNo.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"udid"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"src"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"iOS\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"token"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"1\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         /*
                          {
                          "status": "success",
                          "data": [
                          {
                          "id": "7",
                          "username": "",
                          "password": "1477969b46708cb5173d5d9a9385ce4c993938ba",
                          "first_name": "Aman",
                          "last_name": "K",
                          "ref_number": "123231",
                          "id_number": "",
                          "email": "aman@ideainfoline.com",
                          "phone": "",
                          "is_coach": "0",
                          "is_paid": "0",
                          "coach_id": "0",
                          "udid": "",
                          "status": "active",
                          "lastvisit": "0",
                          "registered": "1455203155",
                          "online": "0",
                          "is_delete": "0",
                          "activation": ""
                          }
                          ],
                          "resultText": "Success"
                          }
                          */
                         NSDictionary *dictTemp = [[response objectForKey:@"data"] objectAtIndex:0];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[dictTemp objectForKey:@"id"]] forKey:@"user_id"];//KUserId
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"email"] forKey:@"email"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_coach"] forKey:@"is_coach"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"first_name"] forKey:@"first_name"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"last_name"] forKey:@"last_name"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_paid"] forKey:@"is_paid"];
                         [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"id_number"] forKey:@"id_number"];

                       
                             MainCoachViewController *objSearchView = (MainCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainCoachViewControllerId"];
                             
                             [self.navigationController pushViewController:objSearchView animated:YES];
                             objSearchView = nil;

                                                 
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    [uploadTask resume];
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtFN) {
        [txtFN resignFirstResponder];
        [txtLN becomeFirstResponder];
    }
    else if (textField == txtLN)
    {
        [txtLN resignFirstResponder];
        [txtRcode becomeFirstResponder];
    }
    else if (textField == txtRcode)
    {
        [txtRcode resignFirstResponder];
        [txtIdNo becomeFirstResponder];
    }
    else if (textField == txtIdNo)
    {
        [txtIdNo resignFirstResponder];
        [txtEmail becomeFirstResponder];
    }
    else if (textField == txtEmail)
    {
        [txtEmail resignFirstResponder];
        [txtPwd becomeFirstResponder];
    }
    else if (textField == txtPwd)
    {
        [txtPwd resignFirstResponder];
        [txtCwd becomeFirstResponder];
    }
    else if (textField == txtCwd)
    {
        [textField resignFirstResponder];
        
    }

    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
