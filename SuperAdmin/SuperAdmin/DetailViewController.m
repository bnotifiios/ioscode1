//
//  DetailViewController.m
//  NotificationPOC
//
//  Created by RnF-12 on 15/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "DetailViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "JSONHelper.h"
#import "Constant.h"
#import "UtilityMethods.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "WebViewController.h"
#import "AppDelegate.h"
#import "PlayViewController.h"
#import "DetailViewController2.h"

@interface DetailViewController ()
{
    NSString *strVideoUrl;
    IBOutlet UIImageView *imgView;
    IBOutlet UIWebView *webViewDoc;
    NSMutableArray *arrInteractiveData;
   // IBOutlet UIButton *btnBack;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;

    int intPlayerHeight,intPlayerWidth;

}
@end

@implementation DetailViewController

@synthesize dictData;


- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(backAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage imageNamed:@"back-icon.png"] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor blackColor]];
    
    button.frame = CGRectMake(8.0, 22.0, 33.0, 34.0);
    
    [self.view addSubview:button];
    
}

- (void)viewDidLoad
{
    intPlayerHeight = 0;
    intPlayerWidth = 0;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"dictData %@",dictData);
    
    arrInteractiveData = [[NSMutableArray alloc] init];
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"photo"])
    {
        
        UIImageView *imgPhoto;
        
        if(IS_IPHONE_5)
        {
            imgPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, 320, 498)];
            
        }
        else if(IS_IPHONE_6)
        {
            imgPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, 375, 597)];
            
            
        }
        else if(IS_IPHONE_6P)
        {
            imgPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, 414, 698)];
            
        }
        else
        {
            imgPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, 320, 410)];
            
        }
        
        imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
        
        [imgPhoto
         sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]]]
         placeholderImage:[UIImage imageNamed:@"no-image.png"]];
        [self.view addSubview:imgPhoto];
        
    }
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"swf"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video not supported" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"audio"])
    {
        
        [self VideoPlayButtonAction:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]]];
    }
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"video"])
    {
        if([[[dictData valueForKey:@"media"] pathExtension] isEqualToString:@"flv"])
        {
            
            NSString *strUrl= [NSString stringWithFormat:@"%@/%@/mobile/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]];
            
            NSString *theFileName = [strUrl stringByDeletingPathExtension];
            
            theFileName = [NSString stringWithFormat:@"%@.mp4",theFileName];
            [self VideoPlayButtonAction:theFileName];
        }
        else
        {
            [self VideoPlayButtonAction:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]]];
        }
    }
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"interactive"])
    {
        
        [self getInteractiveData];
        
        if([[[dictData valueForKey:@"media"] pathExtension] isEqualToString:@"flv"])
        {
            
            NSString *strUrl= [NSString stringWithFormat:@"%@/%@/mobile/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]];
            
            NSString *theFileName = [strUrl stringByDeletingPathExtension];
            
            theFileName = [NSString stringWithFormat:@"%@.mp4",theFileName];
            
            [self VideoPlayButtonAction:theFileName];
        }
        else
        {
            [self VideoPlayButtonAction:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]]];
        }
    }
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"text"])
    {
        
        UITextView *imgPhoto ;
        
        if(IS_IPHONE_5)
        {
            imgPhoto = [[UITextView alloc] initWithFrame:CGRectMake(0, 70, 320, 498)];
            
        }
        else if(IS_IPHONE_6)
        {
            imgPhoto = [[UITextView alloc] initWithFrame:CGRectMake(0, 70, 375, 597)];
            
            
        }
        else if(IS_IPHONE_6P)
        {
            imgPhoto = [[UITextView alloc] initWithFrame:CGRectMake(0, 70, 414, 698)];
            
        }
        else
        {
            imgPhoto = [[UITextView alloc] initWithFrame:CGRectMake(0, 70, 320, 410)];
            
        }
        
        imgPhoto.text = [dictData valueForKey:@"description"];
        
        [self.view addSubview:imgPhoto];
    }
    
    if ([[dictData valueForKey:@"media_type"] isEqualToString:@"walkout"])
    {
        if([[[dictData valueForKey:@"media"] pathExtension] isEqualToString:@"flv"])
        {
            NSString *strUrl= [NSString stringWithFormat:@"%@/%@/mobile/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]];
            
            NSString *theFileName = [strUrl stringByDeletingPathExtension];
            
            theFileName = [NSString stringWithFormat:@"%@.mp4",theFileName];
            [self VideoPlayButtonAction:theFileName];
            
        }
        else
        {
            [self VideoPlayButtonAction:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictData valueForKey:@"media_type"],[dictData valueForKey:@"media"]]];
        }
        
        
    }
    
    [self submitView];
    
}

#pragma mark API Data

-(void)getInteractiveData
{
//[dictData valueForKey:@"media_type"]

NSLog(@"getInteractiveData");

NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KInteractiveButtonUrl]];

[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
[request setHTTPShouldHandleCookies:NO];
[request setTimeoutInterval:60];
[request setHTTPMethod:@"POST"];

NSString *boundary = @"unique-consistent-string";

// set Content-Type in HTTP header
NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
[request setValue:contentType forHTTPHeaderField: @"Content-Type"];

// post body
NSMutableData *body = [NSMutableData data];
//user_id, user_media_id
// add params (all params are strings)
[body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
[body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];


[body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];

// setting the body of the post to the reqeust
[request setHTTPBody:body];

// set the content-length
NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
[request setValue:postLength forHTTPHeaderField:@"Content-Length"];

NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];

NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];

NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                    
                                    {
                                        
                                        if(data.length > 0)
                                        {
                                            //success
                                            NSLog(@"success");
                                            
                                            NSError *jsonError = nil;
                                            
                                        if (data != nil) {
                                            
                                            NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                            NSLog(@"success %@",response);
                                            
                                            if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                                            {
                                                if ([[response objectForKey:@"data"]count]>0)
                                                {
                                                    strVideoUrl = [response objectForKey:@"datasource"];
                                                    
                                                    
                                                    [arrInteractiveData removeAllObjects];
                                                    
                                                    arrInteractiveData = [response objectForKey:@"data"];
                                                    
                                    
                                                    if([arrInteractiveData count]>0)
                                                    {
                                                        //check video height & width
                                                        
                                                        if([[dictData valueForKey:@"media_width"] intValue] > [[dictData valueForKey:@"media_height"] intValue])
                                                        {
                                                        for(int i = 0; i<[arrInteractiveData count];i++)
                                                        {
                                                            NSDictionary *dictTemp = [arrInteractiveData objectAtIndex:i];
                                                            
                                                            UIButton *but= [UIButton buttonWithType:UIButtonTypeRoundedRect];
                                                            
                                                            [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                                                            but.tag = i;
                                                            
                                                            
                                                            float xVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"position_x"] floatValue];
                                                            
                                                            float yVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"position_y"] floatValue]+56;
                                                            
                                                            float wVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"width"] floatValue];
                                                            
                                                            float hVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"height"] floatValue];
                                                            
                                                            [but setFrame:CGRectMake(xVal, yVal, wVal, hVal)];
                                                            NSLog(@"xVal-%f yVal-%f wVal-%f hVal-%f",xVal, yVal, wVal, hVal);
                                                            
                                                            [but setTitle:[dictTemp valueForKey:@"button_label"] forState:UIControlStateNormal];
                                                            [but setExclusiveTouch:YES];
                                                           
                                                            if([[dictTemp valueForKey:@"button_image"] length] >0)
                                                            {
                                                                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@button_image/%@",strVideoUrl,[dictTemp valueForKey:@"button_image"]]];
                                                                NSData *data = [NSData dataWithContentsOfURL:url];
                                                                UIImage *image = [UIImage imageWithData:data];
                                                                
                                                    [but setImage:image forState:UIControlStateNormal];
                                                            }
                                                            
                                                            [but setBackgroundColor:[self colorFromHexString:[dictTemp valueForKey:@"button_color"]]];
                                                            
                                                            [self.view addSubview:but];
                                                            
                                                        }
                                                        }
                                                        else
                                                        {
                                                            for(int i = 0; i<[arrInteractiveData count];i++)
                                                            {
                                                                NSDictionary *dictTemp = [arrInteractiveData objectAtIndex:i];
                                                                
                                                                UIButton *but= [UIButton buttonWithType:UIButtonTypeRoundedRect];
                                                                
                                                                [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                                                                but.tag = i;
                                                                
                                                                //mobile_player_width / (600 * (video_width/video_height));
                                                                
                                                                float scale_factor  = ([[UIScreen mainScreen] bounds].size.width)/(600 * ([[dictData valueForKey:@"media_width"] floatValue]/[[dictData valueForKey:@"media_height"] floatValue]));
                                                                
                                                                int xVal = scale_factor * [[dictTemp valueForKey:@"position_x"] floatValue];
                                                                
                                                                int yVal = scale_factor * [[dictTemp valueForKey:@"position_y"] floatValue] + 56;
                                                                
                                                                int wVal = scale_factor*[[dictTemp valueForKey:@"width"] floatValue];
                                                                
                                                                int hVal = scale_factor*[[dictTemp valueForKey:@"height"] floatValue];
                                                                
                                                                [but setFrame:CGRectMake(xVal, yVal, wVal, hVal)];
                                                                NSLog(@"xVal = %d yVal=%d wVal=%d hVal=%d",xVal, yVal, wVal, hVal);
                                                                [but setTitle:[dictTemp valueForKey:@"button_label"] forState:UIControlStateNormal];
                                                                [but setExclusiveTouch:YES];
                                                                
                                                                if([[dictTemp valueForKey:@"button_image"] length] >0)
                                                                {
                                                                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://bnotifi.com/super/media/media_type/interactive/button_image/%@",[dictTemp valueForKey:@"button_image"]]];
                                                                    NSData *data = [NSData dataWithContentsOfURL:url];
                                                                    UIImage *image = [UIImage imageWithData:data];
                                                                    
                                                                    [but setBackgroundImage:image forState:UIControlStateNormal];
                                                                }
                                                                
                                                                [but setBackgroundColor:[self colorFromHexString:[dictTemp valueForKey:@"button_color"]]];
                                                                
                                                                [self.view addSubview:but];
                                                                
                                                            }
  
                                                        }
                                                    }
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    
                                                }
                                                
                                            }
                                            else
                                            {
                                                NSLog(@"failed");
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                [alert show];
                                            }
                                        }
                                            
                                        }
                                    }];
[uploadTask resume];


}

#pragma mark colorFromHexString

- (UIColor *)colorFromHexString:(NSString *)hexString
{
    NSString *cString = [[hexString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void) buttonClicked:(UIButton*)sender
{
    for (UIView *i in self.view.subviews)
    {
        if(i.tag == 11){
            
            NSLog(@"playerItemDidReachEnd");
            
            [i removeFromSuperview];
            break;
            //i = nil;
        }
    }
    
    NSDictionary *dictTemp = [arrInteractiveData objectAtIndex:sender.tag];
    
    NSLog(@"you clicked on button %ld -- %@", (long)sender.tag,dictTemp);

    if([[dictTemp valueForKey:@"button_type"] isEqualToString:@"2"])
    {
        
       
    if([[[dictTemp valueForKey:@"button_media"] pathExtension] isEqualToString:@"flv"])
    {
        
        NSString *strUrl= [NSString stringWithFormat:@"%@mobile/%@",strVideoUrl,[dictTemp valueForKey:@"button_media"]];
        
        NSString *theFileName = [strUrl stringByDeletingPathExtension];
        
        theFileName = [NSString stringWithFormat:@"%@.mp4",theFileName];
        
        PlayViewController *objVC = (PlayViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"PlayViewControllerId"];
        objVC.strUrl = theFileName;
        
        objVC.strTitle = [dictTemp valueForKey:@"button_label"];
        
        [self.navigationController pushViewController:objVC animated:YES];
        objVC = nil;

        
    }
    else
    {
        PlayViewController *objVC = (PlayViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"PlayViewControllerId"];
        objVC.strUrl = [NSString stringWithFormat:@"%@%@",strVideoUrl,[dictTemp valueForKey:@"button_media"]];
        objVC.strTitle = [dictTemp valueForKey:@"button_label"];
        
        [self.navigationController pushViewController:objVC animated:YES];
        objVC = nil;
        
    }
    }
    else if([[dictTemp valueForKey:@"button_type"] isEqualToString:@"7"])//survey
    {
        
        WebViewController *objVC = (WebViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
        objVC.strUrl = [NSString stringWithFormat:@"%@/%@",[dictTemp valueForKey:@"button_media"],[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]];
        objVC.strTitle = [dictTemp valueForKey:@"button_label"];
        objVC.strMediaType = @"notification";
        objVC.dictData = dictData;
        
        [self.navigationController pushViewController:objVC animated:YES];
        objVC = nil;
        
    }
    else if([[dictTemp valueForKey:@"button_type"] isEqualToString:@"8"])//inteactive
    {
        DetailViewController2 *objRegisterView = (DetailViewController2*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"DetailViewController2Id"];
        
        objRegisterView.dictData = dictTemp;
        [self.navigationController pushViewController:objRegisterView animated:YES];
        objRegisterView = nil;
        
    }
    else
    {
        WebViewController *objVC = (WebViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
        objVC.strUrl = [NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"button_media"]];
        objVC.strTitle = [dictTemp valueForKey:@"button_label"];
        objVC.strMediaType = @"notification";
        objVC.dictData = dictData;

        [self.navigationController pushViewController:objVC animated:YES];
        objVC = nil;

    }
    
}


-(void)submitView
{
    NSLog(@"submitView");

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseInsertMediaViewUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
        
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_view_type"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"notification\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];


    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
     [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];

    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"src"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"ios\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"user_media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                                                 
                     }
                     else
                     {
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                     [alert show];
                 }
             }
             
         }
     }];
    
}

#pragma mark Video Play

-(void)VideoPlayButtonAction:(NSString*)strUrl
{
    NSLog(@"VideoPlayButtonAction");
    
    if ([strUrl length]>0)
    {
        NSString *urlString =
        [NSString stringWithFormat:@"%@",
         [strUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        
        // asset is a video
        AVPlayer *avPlayer = [[AVPlayer alloc] initWithURL:[NSURL URLWithString:urlString]];
        //avPlayer.muted = YES;
        
        NSLog(@"Video url %@",urlString);
        
        AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avPlayer];
        UIView *subView;
        
        [subView setBackgroundColor:[UIColor redColor]];
        [avPlayerLayer setBackgroundColor:(__bridge CGColorRef _Nullable)([UIColor greenColor])];
        
        if ([[dictData valueForKey:@"media_type"] isEqualToString:@"interactive"])
        {
            
            if([[dictData valueForKey:@"media_width"] intValue]>[[dictData valueForKey:@"media_height"] intValue])
            {
                
                intPlayerWidth = [[UIScreen mainScreen] bounds].size.width;
                
                intPlayerHeight = ([[dictData valueForKey:@"media_height"] intValue] * intPlayerWidth ) / [[dictData valueForKey:@"media_width"] intValue];
                
            }
            else
            {
                intPlayerHeight = [[UIScreen mainScreen] bounds].size.height;
                
                intPlayerWidth = ([[dictData valueForKey:@"media_width"] intValue] * intPlayerHeight) / [[dictData valueForKey:@"media_height"] intValue];
            }

            
//            intPlayerHeight = ((([[UIScreen mainScreen] bounds].size.width)/4)*3);
//            intPlayerWidth = ((([[UIScreen mainScreen] bounds].size.width)/4)*3);
            
            [avPlayerLayer setFrame:CGRectMake(0, 0, intPlayerWidth, intPlayerHeight)];
            subView = [[UIView alloc]initWithFrame:CGRectMake(0, 56, intPlayerWidth, intPlayerHeight)];
            /*
            if(IS_IPHONE_5)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, ([[UIScreen mainScreen] bounds].size.width), intPlayerHeight)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width), 568)];
            }
            else if(IS_IPHONE_6)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, ([[UIScreen mainScreen] bounds].size.width), intPlayerHeight)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width), 667)];
                
            }
            else if(IS_IPHONE_6P)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, ([[UIScreen mainScreen] bounds].size.width), intPlayerHeight)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width), 768)];
                
            }
            else
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, ([[UIScreen mainScreen] bounds].size.width), intPlayerHeight)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width), 480)];
                
            }
             */
            
            //avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        }
        else
        {
            if(IS_IPHONE_5)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 320, 568)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
            }
            else if(IS_IPHONE_6)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 375, 667)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 375, 667)];
                
            }
            else if(IS_IPHONE_6P)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 414, 768)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 414, 768)];
                
            }
            else
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 320, 480)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
                
            }
            //avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        }
        
        //avPlayerLayer.anchorPoint = CGPointMake(0.5, 0);
        subView.tag = 11;
        [subView.layer addSublayer:avPlayerLayer];
        
        [self.view addSubview:subView];
        
        [avPlayer play];
        
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[avPlayer currentItem]];
        
    }
    else
    {
        NSLog(@"Video not available");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video not available" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    
    
    //    AVPlayerItem *p = [notification object];
    //    [p seekToTime:kCMTimeZero];
    //    p = nil;
    
    for (UIView *i in self.view.subviews)
    {
        if(i.tag == 11){
            
            NSLog(@"playerItemDidReachEnd");
            
            //[i removeFromSuperview];
            break;
            //i = nil;
        }
    }
}

@end
