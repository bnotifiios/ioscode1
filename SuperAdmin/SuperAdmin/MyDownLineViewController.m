//
//  MyDownLineViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 30/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "MyDownLineViewController.h"
#import "Constant.h"
#import "DownlineTableViewCell.h"


@interface MyDownLineViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *tblList;
    NSMutableArray *arrList;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    IBOutlet UILabel *lblTitle;

}


@end

@implementation MyDownLineViewController

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    
    lblTitle.text = _topTitle;
    arrList = [NSMutableArray new];

    [self CallAPI];

}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 110;
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [arrList count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"DownlineTableViewCell";
    
    DownlineTableViewCell *cell1 = (DownlineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell1 == nil)
    {
        NSArray *nib ;
        
        if (IS_IPHONE_5)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"DownlineTableViewCell" owner:self options:nil];
            
        }
        else if (IS_IPHONE_6)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"DownlineTableViewCell-iPhone6" owner:self options:nil];
            
        }
        else if (IS_IPHONE_6P)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"DownlineTableViewCell-iPhone6P" owner:self options:nil];
            
        }else if (IS_IPHONE_4S)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"DownlineTableViewCell" owner:self options:nil];
            
        }
        cell1 = [nib objectAtIndex:0];
        
        [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    //cell1.viewWhite.layer.cornerRadius = 3.0f;
    
    NSDictionary *dictTemp = [arrList objectAtIndex:indexPath.row];
    
    cell1.lblName.text = [NSString stringWithFormat:@"%@,%@",[dictTemp valueForKey:@"last_name"],[dictTemp valueForKey:@"first_name"]];
    if([[dictTemp valueForKey:@"is_coach"] isEqualToString:@"0"])
    {
        cell1.lblType.text = @"Customer";
    }
    else
    {
        cell1.lblType.text = @"Coach";

    }
    cell1.lblStatus.text = [dictTemp valueForKey:@"status"];

    return cell1;
}


#pragma mark API Methods

-(void)CallAPI
{
    NSLog(@"CallAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseDownlineUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"1\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         arrList = [response objectForKey:@"data"];
                         [tblList reloadData];
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You currently do not have anyone in your down line" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         alert.tag = 10;
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag ==10)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
