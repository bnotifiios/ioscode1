//
//  AppDelegate.m
//  SuperAdmin
//
//  Created by Gauri Shankar on 02/10/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import "AppDelegate.h"
#import "JSONHelper.h"
#import "Constant.h"
#import "UtilityMethods.h"
#import "HomeViewController.h"
#import "PayPalMobile.h"
#import "NotificationCoachViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize dictProject;

+ (AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    NSString *cString = [[hexString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+ (UIStoryboard *)storyBoardType
{
    UIStoryboard *storyboard;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        
        if (IS_IPHONE_5)
        {
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone5
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            NSLog(@"loaded iPhone5 Storyboard");
        }
        else if (IS_IPHONE_6)
        {   // iPhone 3GS, 4, and 4S and iPod Touch 3rd and 4th generation: 3.5 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            storyboard = [UIStoryboard storyboardWithName:@"Main-iPhone6" bundle:nil];
            
            NSLog(@"loaded Main_iPhone6 Storyboard");
        }
        else if (IS_IPHONE_6P)
        {   // iPhone 3GS, 4, and 4S and iPod Touch 3rd and 4th generation: 3.5 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            storyboard = [UIStoryboard storyboardWithName:@"Main-iPhone6P" bundle:nil];
            
            NSLog(@"loaded Main_iPhone6P Storyboard");
        }
        else
        {   // iPhone 3GS, 4, and 4S and iPod Touch 3rd and 4th generation: 3.5 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            storyboard = [UIStoryboard storyboardWithName:@"Main-iPhone4" bundle:nil];
            //storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            NSLog(@"loaded iPhone4 Storyboard");
        }
        
    }
    
    
    return storyboard;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
     [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"Ac6JL86ES5nZfPlGpkmP-3UoK0Qu2_Q_9ZekxwX050MAzRBPBykzERoEwmvGjUcVjjlayOdeWgDx6tCT"}];
    
    // Register for Push Notitications, if running iOS 8
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
        NSLog(@"dbg: #### Remote notification (iOS8 style) ON ####");
    } else {
        // Register for Push Notifications before iOS 8
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
        NSLog(@"dbg: #### Remote notification (iOS7 style) ON ####");
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] length] == 0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"86b7bd34b94203aac7677ea035f0f4db499730e32848b2a88bc1473b577bd481" forKey:@"pushToken"];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"status"];//KUserId

    UIViewController *viewController;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"] length] == 0)
    {
        viewController = [[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProjectCodeVCId"];
        
    }
    else
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_mlm"] isEqualToString:@"0"])
        {
           
            viewController = [[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainViewControllerId"];
            
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_coach"] isEqualToString:@"0"])
            {
                
                viewController = [[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"NewCustomerViewControllerId"];
            }
            else
            {
                viewController = [[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MainCoachViewControllerId"];

            }
        }
        
    }
    
    UINavigationController *navController1 = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    self.window.rootViewController = navController1;
    [self.window makeKeyAndVisible];
    

    return YES;
}

#pragma mark - Push Notification Methods -

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString *pushToken = [[[[deviceToken description]
                             stringByReplacingOccurrencesOfString:@"<"withString:@""]
                            stringByReplacingOccurrencesOfString:@">" withString:@""]
                           stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:pushToken forKey:@"pushToken"];
    
    NSLog(@"%@", pushToken);
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *strErr = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"strErr = %@",strErr);
}


-(void)application:(UIApplication*)app didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //app.applicationIconBadgeNumber = 0;
    NSLog(@"userInfo %@",userInfo);
    
    for (id key in userInfo)
    {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    app.applicationIconBadgeNumber = 1;
    
    NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        NSLog(@"Notification received by running app");
    }
    else
    {
        NSLog(@"App opened from Notification");
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        UIViewController *viewController;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"] length] == 0)
        {
            viewController = (HomeViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"HomeViewControllerId"];
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"APNS"];//KUserId
            
            viewController = (NotificationCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"NotificationCoachViewControllerId"];
            
        }
        
        UINavigationController *navController1 = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        [self.window setBackgroundColor:[UIColor whiteColor]];
        
        self.window.rootViewController = navController1;
        [self.window makeKeyAndVisible];
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
