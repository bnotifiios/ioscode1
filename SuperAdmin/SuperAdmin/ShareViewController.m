//
//  ShareViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 14/05/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()
{
    IBOutlet UIWebView *webViewUrl;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;

}
@end

@implementation ShareViewController

@synthesize dictData;

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURL *url = [NSURL URLWithString:dictData];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webViewUrl loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
