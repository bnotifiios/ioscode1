//
//  NotificationCoachViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 21/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "NotificationCoachViewController.h"
#import "JSONHelper.h"
#import "Constant.h"
#import "UtilityMethods.h"
#import "DetailViewController.h"
#import "MBProgressHUD.h"
#import "ListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ShareViewController.h"
#import "AppDelegate.h"


@interface NotificationCoachViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *tblList,*tblDownline;
    NSMutableArray *arrList,*arrDownline,*arrSelectionStatus;
    IBOutlet UIButton *btnGallery,*btnSent,*btnReceived;
    NSString *strOption,*strShareUrl;
    IBOutlet UIView *viewList;
    NSDictionary *dictGlobalData;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    IBOutlet UILabel *lblTitle,*lblGallery,*lblSent,*lblReceived;

}

@property(strong)  NSIndexPath* lastIndexPath;

@end

@implementation NotificationCoachViewController


- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    [viewList setHidden:YES];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_mlm"] isEqualToString:@"0"])
    {
        strOption = @"Received";
        [btnSent setHidden:YES];
        [btnReceived setHidden:YES];
        [btnGallery setHidden:NO];
        //[btnGallery setTitle:@"RECEIVED" forState:UIControlStateNormal];
        [btnGallery setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"received_text"] forState:UIControlStateNormal];

        [self getList];
    }
    else
    {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 0)
    {
        strOption = @"Received";
        [btnSent setHidden:YES];
        [btnReceived setHidden:YES];
        [btnGallery setHidden:NO];
        //[btnGallery setTitle:@"RECEIVED" forState:UIControlStateNormal];
        [btnGallery setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"received_text"] forState:UIControlStateNormal];

        [self getList];

    }
    else
    {
        [btnGallery setBackgroundImage:[UIImage imageNamed:@"gallery-selected.png"] forState:UIControlStateNormal];
        [btnReceived setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
        [btnSent setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
        
        [btnSent setHidden:NO];
        [btnReceived setHidden:NO];
        [btnGallery setHidden:NO];
        //[btnGallery setTitle:@"GALLERY" forState:UIControlStateNormal];
        [btnGallery setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"gallery_text"] forState:UIControlStateNormal];

        strOption = @"Gallery";
        [self getGallery];

    }
    }
    
}

- (void)viewDidLoad
{
    self.title = @"NOTIFICATIONS";
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    lblTitle.text = _topTitle;
    arrList = [[NSMutableArray alloc] init];
    arrDownline = [[NSMutableArray alloc] init];
    arrSelectionStatus  = [[NSMutableArray alloc] init];
    strOption = @"Gallery";

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [tblList addSubview:refreshControl];
    
    [btnGallery setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"gallery_text"] forState:UIControlStateNormal];
    [btnReceived setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"received_text"] forState:UIControlStateNormal];
    [btnSent setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"sent_text"] forState:UIControlStateNormal];

}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)cancelAction:(id)sender
{
    [viewList setHidden:YES];
}

//Send notification to ind. user

-(IBAction)sendDownlineAction:(id)sender
{
    if([arrDownline count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please select any downline user!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    [self callSendIndividualUser];
}
}

-(IBAction)receivedAction:(id)sender
{

    strOption = @"Received";

    [btnGallery setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
    [btnReceived setBackgroundImage:[UIImage imageNamed:@"gallery-selected.png"] forState:UIControlStateNormal];
    [btnSent setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
    
    [self getList];
    
}

-(IBAction)sentAction:(id)sender
{
    strOption = @"Sent";

    [btnGallery setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
    [btnReceived setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
    [btnSent setBackgroundImage:[UIImage imageNamed:@"gallery-selected.png"] forState:UIControlStateNormal];
    
    //To use this feature you must first go back to the main menu and Upgrade your membership

    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 0)
    {
        [arrList removeAllObjects];
        [tblList reloadData];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"To use this feature you must first go back to the main menu and Upgrade your membership!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self getSent];
    }
}


-(IBAction)galleryAction:(id)sender
{
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 0)
    {
        strOption = @"Received";
        [btnSent setHidden:YES];
        [btnReceived setHidden:YES];
        [btnGallery setHidden:NO];
        //[btnGallery setTitle:@"RECEIVED" forState:UIControlStateNormal];
        [btnGallery setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"received_text"] forState:UIControlStateNormal];
        [self getList];
        
    }
    else
    {
        [btnGallery setBackgroundImage:[UIImage imageNamed:@"gallery-selected.png"] forState:UIControlStateNormal];
        [btnReceived setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
        [btnSent setBackgroundImage:[UIImage imageNamed:@"gallery.png"] forState:UIControlStateNormal];
        
        [btnSent setHidden:NO];
        [btnReceived setHidden:NO];
        [btnGallery setHidden:NO];
        //[btnGallery setTitle:@"GALLERY" forState:UIControlStateNormal];
        [btnGallery setTitle:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"gallery_text"] forState:UIControlStateNormal];

        strOption = @"Gallery";
        [self getGallery];
        
    }
}



- (void)refresh:(UIRefreshControl *)refreshControl
{
    if([strOption isEqualToString:@"Gallery"])
    {
        [self getGallery];
    }
    
    if([strOption isEqualToString:@"Sent"])
    {
        [self getSent];
    }
    
    if([strOption isEqualToString:@"Received"])
    {
        [self getList];
    }

    // Do your job, when done:
    [refreshControl endRefreshing];
}

#pragma mark API

-(void)getSent
{
    NSLog(@"Get getSent");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseSentNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"is_coach"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if([arrList count]>0)
                     {
                         [arrList removeAllObjects];
                     }

                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         arrList = [response objectForKey:@"data"];

                          [tblList reloadData];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     
                     if([arrList count]>0)
                     {
                         [arrList removeAllObjects];
                         [tblList reloadData];
                         
                     }

                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                     [alert show];
                 }
             }
             
         }
     }];
    
}

-(void)getGallery
{
    NSLog(@"Get getGallery");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KGalleryUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"is_coach"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             //NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if([arrList count]>0)
                     {
                         [arrList removeAllObjects];
                     }

                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         strShareUrl =[response objectForKey:@"shareurl"];
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
                     }

                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                        
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     
                     if([arrList count]>0)
                     {
                         [arrList removeAllObjects];
                         [tblList reloadData];

                     }
                     
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                     [alert show];
                 }
             }
             
         }
     }];
    
}

-(void)getList
{
    NSLog(@"Get List");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KReceivedNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if([arrList count]>0)
                     {
                         [arrList removeAllObjects];
                     }

                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     
                     if([arrList count]>0)
                     {
                         [arrList removeAllObjects];
                         [tblList reloadData];
                         
                     }

                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                     [alert show];
                 }
             }
             
         }
     }];
    
}

#pragma mark API Methods

-(void)callSendIndividualUser
{
    NSLog(@"callSendIndividualUser");
    
    NSMutableArray *arrJoin = [[NSMutableArray alloc] init];
    
    for(int i = 0;i<[arrSelectionStatus count];i++)
    {
        
        if ([[arrSelectionStatus objectAtIndex:i] boolValue] == YES)
        {
            [arrJoin addObject:[[arrDownline objectAtIndex:i] valueForKey:@"id"]];
        }
        
    }
    
    NSString *joinedComponents = [arrJoin componentsJoinedByString:@","];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseSendNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"1\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictGlobalData valueForKey:@"media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"receivers"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",joinedComponents] dataUsingEncoding:NSUTF8StringEncoding]];
   
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         [viewList setHidden:YES];

                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification sent to downline users!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         alert.tag = 31;
                         [alert show];
                         
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

-(void)callSendAllNotification
{
    NSLog(@"callSendAllNotification");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseSendNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"1\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictGlobalData valueForKey:@"media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification sent to all downline users!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];

                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

-(void)CallAPI
{
    NSLog(@"CallAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseDownlineUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"is_coach"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"1\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         arrDownline = (NSMutableArray*)[response objectForKey:@"data"];
                         
                         //arrSelectionStatus =[NSMutableArray array]; //arrSelectionStatus holds the cell selection status
                         for (int i=0; i<arrDownline.count; i++)
                         { //arrElements holds those elements which will be populated in tableview
                             [arrSelectionStatus addObject:[NSNumber numberWithBool:NO]];
                         }
                         
                         [tblDownline reloadData];

                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

#pragma mark Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tblDownline)
    {
        return 45;
        
    }
    else
    {
        return 76;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == tblDownline)
    {
        return [arrDownline count];

    }
    else
    {
    return [arrList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tblDownline)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        
        NSDictionary *dictTemp = [arrDownline objectAtIndex:indexPath.row];
        
        cell.textLabel.text= [NSString stringWithFormat:@"%@,%@",[dictTemp valueForKey:@"last_name"],[dictTemp valueForKey:@"first_name"]];
        
        if ([[arrSelectionStatus objectAtIndex:indexPath.row] boolValue] == YES)
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"ListTableViewCell";
        
        ListTableViewCell *cell1 = (ListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        
        if (cell1 == nil)
        {
            NSArray *nib ;
            
            if (IS_IPHONE_5)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"ListTableViewCell" owner:self options:nil];
                
            }
            else if (IS_IPHONE_6)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"ListTableViewCell-iPhone6" owner:self options:nil];
                
            }
            else if (IS_IPHONE_6P)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"ListTableViewCell-iPhone6P" owner:self options:nil];
                
            }else if (IS_IPHONE_4S)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"ListTableViewCell" owner:self options:nil];
                
            }
            cell1 = [nib objectAtIndex:0];
            
            [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        // Set the data for this cell
        
        NSDictionary *dictTemp = [arrList objectAtIndex:indexPath.row];
        
        cell1.lblTitle.text = [dictTemp valueForKey:@"title"];
        cell1.lblDate.text = [dictTemp valueForKey:@"date_created"];
        
        cell1.btnView.tag = indexPath.row;
        [cell1.btnView addTarget:self
                          action:@selector(viewAction:)
                forControlEvents:UIControlEventTouchUpInside];
        
        cell1.btnDelete.tag = indexPath.row;
        [cell1.btnDelete addTarget:self
                            action:@selector(deleteAction:)
                  forControlEvents:UIControlEventTouchUpInside];
        
        if([strOption isEqualToString:@"Gallery"])
        {
            [cell1.btnShare setHidden:NO];
            [cell1.btnSend setHidden:NO];
            [cell1.btnView setHidden:YES];
            [cell1.btnDelete setHidden:YES];
            
            cell1.btnShare.tag = indexPath.row;
            [cell1.btnShare addTarget:self
                               action:@selector(shareAction:)
                     forControlEvents:UIControlEventTouchUpInside];
            cell1.btnSend.tag = indexPath.row;
            [cell1.btnSend addTarget:self
                              action:@selector(sendAction:)
                    forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        
        if([strOption isEqualToString:@"Received"])
        {
            [cell1.btnShare setHidden:YES];
            [cell1.btnSend setHidden:YES];
            [cell1.btnView setHidden:NO];
            [cell1.btnDelete setHidden:NO];
            
        }
        if([strOption isEqualToString:@"Sent"])
        {
            [cell1.btnShare setHidden:YES];
            [cell1.btnSend setHidden:YES];
            [cell1.btnView setHidden:NO];
            [cell1.btnDelete setHidden:NO];
            
        }
        
        if([[dictTemp valueForKey:@"is_watch"] isEqualToString:@"1"])
        {
            [cell1.imgView setHidden:YES];
        }
        
        [cell1.imgProduct
         sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictTemp valueForKey:@"media_type"],[dictTemp valueForKey:@"media_thumb"]]]
         placeholderImage:[UIImage imageNamed:@"no-image.png"]];
        
        
        return cell1;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tblDownline)
    {

        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if ([[arrSelectionStatus objectAtIndex:indexPath.row] boolValue] == NO)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arrSelectionStatus replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:YES]];

        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [arrSelectionStatus replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];

        }
        

    }
    else
    {
    DetailViewController *objRegisterView = (DetailViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"DetailViewControllerId"];
    
    objRegisterView.dictData = [arrList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:objRegisterView animated:YES];
    objRegisterView = nil;
    }
}

-(void)sendAction:(UIButton*)sender
{
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 0)
    {

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"To use this feature you must first go back to the main menu and Upgrade your membership" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = sender.tag;
        [alert show];
    }
    else
    {
        NSLog(@"downline user");
        
        dictGlobalData = [arrList objectAtIndex:sender.tag];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Send to downline" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Send to All",@"Send to Individual",nil];
        alert.tag = 30;
        [alert show];
    }
}

-(void)shareAction:(UIButton*)sender
{
    NSLog(@"shareAction");
    
    NSDictionary *dictTemp = [arrList objectAtIndex:sender.tag];
    
    NSArray *activityItems = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Check out this cool content that your friend %@ %@ has shared from %@ %@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"],[[NSUserDefaults standardUserDefaults] objectForKey:@"last_name"],[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"],strShareUrl,[dictTemp valueForKey:@"media_id"]], nil];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    //    [activityVC setValue:[NSString stringWithFormat:@"Click here to get the %@ Application",[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"]] forKey:@"subject"];

    [activityVC setValue:[NSString stringWithFormat:@"Check out this cool content that your friend %@ %@ has shared from My Coaching Business",[[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"],[[NSUserDefaults standardUserDefaults] objectForKey:@"last_name"]] forKey:@"subject"];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         NSLog(@"Activity = %@",activityType);
         NSLog(@"Completed Status = %d",completed);
         
         if (completed)
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Successfully Shared" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
         else
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Unable To Share" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
             
         }
     }];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    

}

-(void)viewAction:(UIButton*)sender
{
    NSLog(@"view Action");
    DetailViewController *objRegisterView = (DetailViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"DetailViewControllerId"];
    
    objRegisterView.dictData = [arrList objectAtIndex:sender.tag];
    [self.navigationController pushViewController:objRegisterView animated:YES];
    objRegisterView = nil;
}


-(void)deleteAction:(UIButton*)sender
{
    dictGlobalData = [arrList objectAtIndex:sender.tag];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You want to delete!" message:@"" delegate:self cancelButtonTitle:@"" otherButtonTitles:@"YES",@"NO", nil];
    alert.tag = 10;
    [alert show];
}



#pragma mark ALert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 10)
    {
        if (buttonIndex == 1)
        {
            if([strOption isEqualToString:@"Received"])
            {
                [self deleteRecordReceived:dictGlobalData];
            }
            else
            {
                [self deleteRecordSent:dictGlobalData];

            }
        }
    }
    if(alertView.tag == 30)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Send to all");
            [self callSendAllNotification];
        }
        else
        {
            NSLog(@"Send to one by one");
            [viewList setHidden:NO];
            [self CallAPI];
        }
    }
    if(alertView.tag == 31)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)deleteRecordReceived:(NSDictionary *)dictData
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseDeleteReceivedNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictGlobalData valueForKey:@"user_media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
                     }
                     else
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}

-(void)deleteRecordSent:(NSDictionary *)dictData
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseDeleteSentNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictGlobalData valueForKey:@"user_media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         arrList = [response objectForKey:@"data"];
                         
                         [tblList reloadData];
                         
                     }
                     else
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No record found!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
