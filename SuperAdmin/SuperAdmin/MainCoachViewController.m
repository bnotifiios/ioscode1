//
//  MainCoachViewController.m
//  NotificationPOC
//
//  Created by Gauri Shankar on 01/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "MainCoachViewController.h"

#import "AppDelegate.h"
#import "ProductsViewController.h"
#import "ServicesViewController.h"
#import "TrainingVideoViewController.h"
#import "ViewController.h"
#import "ChangePasswordViewController.h"
#import "MyDownLineViewController.h"
#import "LoginViewController.h"
#import "NotificationCoachViewController.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "PayPalMobile.h"
#import "MusicViewController.h"
#import "ProjectCodeVC.h"
#import "BecomeACoachViewController.h"

@interface MainCoachViewController ()<UIAlertViewDelegate,PayPalPaymentDelegate>
{
    IBOutlet UILabel *lblProducts,*lblVideo,*lblNotification,*lblChangePwd,*lblShareBusiness;
    IBOutlet UILabel *lblCount,*lblMyTeam,*lblUpgrade,*lblBeComeACoach,*lblDownline;
    
    IBOutlet UIButton *btnDownline,*btnUpgrade,*btnBeComeACoach;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    
    IBOutlet UIView *viewUpgrade;
}

@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;

@end

@implementation MainCoachViewController

- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    //Coach
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 1  && [[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 0)
    {
        [viewUpgrade setHidden:NO];
    }
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 1  && [[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 1)
    {
        [viewUpgrade setHidden:YES];
        
    }
    
    [self CallAPI];
    
}

- (void)viewDidLoad
{
    [lblCount setHidden:YES];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];

    lblProducts.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"products_text"];
    lblVideo.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"video_text"];
    lblNotification.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"notification_text"];
    lblChangePwd.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"changepassword_text"];
    lblUpgrade.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"upgrade_text"];
    lblShareBusiness.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"share_the_business_text"];
    lblBeComeACoach.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"be_a_coach_text"];
    lblDownline.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"my_downline_text"];

}

#pragma mark API Methods

//Get Count
-(void)CallAPI
{
    NSLog(@"CallAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KAcitiveNotifiUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"project_id"]] dataUsingEncoding:NSUTF8StringEncoding]];

    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         if([[response objectForKey:@"count"] isEqualToString:@"0"])
                         {
                             [lblCount setHidden:YES];
                         }
                         else
                         {
                             [lblCount setHidden:NO];
                             
                             lblCount.text = [response objectForKey:@"count"];
                         }
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                     [alert show];
                 }
             }
             
         }
     }];
    
}

-(void)SubmitPayPalDetail:(NSDictionary *)dictData
{
    NSLog(@"SubmitPayPalDetail");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBasePaymentUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"project_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // transaction_id
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"transaction_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData objectForKey:@"id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"payer_email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData objectForKey:@"id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"payment_gross"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"6.00\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"currency_code"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"USD\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"payment_status"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData objectForKey:@"state"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(data.length > 0)
         {
             //success
             NSLog(@"success");
             
             //[MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSError *jsonError = nil;
             
             if (data != nil) {
                 
                 NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                 
                 if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     if ([[response objectForKey:@"data"]count]>0)
                     {
                         NSLog(@"success %@",response);
                         
                         NSDictionary *dictTemp = [[response objectForKey:@"data"] objectAtIndex:0];
                         
                        [[NSUserDefaults standardUserDefaults] setObject:[dictTemp objectForKey:@"is_paid"] forKey:@"is_paid"];

                         //Coach
                         if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 1  && [[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 0)
                         {
                             
                             
                             
                             [viewUpgrade setHidden:NO];
                             
                         }
                         else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 1  && [[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 1)
                         {
                             
                            [viewUpgrade setHidden:YES];
                             
                         }
                         

                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulation, Your payment process to become Coach is done." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     else
                     {
                         //[MBProgressHUD hideHUDForView:self.view animated:YES];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No payment done!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }
                 else
                 {
                     NSLog(@"failed");
                     //[MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }
     }];
    
}


-(IBAction)logOutAction:(id)sender
{
    NSLog(@"loginAction");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
    alert.tag = 20;
    [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 20)
    {
    if(buttonIndex == 0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_id"];//KUserId
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"status"];//KUserId

        ProjectCodeVC *objSearchView = (ProjectCodeVC*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProjectCodeVCId"];
        
        [self.navigationController pushViewController:objSearchView animated:YES];
        objSearchView = nil;
    }
    }
    
    if(alertView.tag == 10)
    {
        if(buttonIndex == 0)
        {
            [self payViaPayPal];
        }
    }
    
    if(alertView.tag == 30)
    {
        [self shareCoachAction];
    }
   
}

#pragma mark Custom methods

-(IBAction)productAction:(id)sender
{
    NSLog(@"productAction");
    
    ProductsViewController *objSearchView = (ProductsViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProductsViewControllerId"];
    objSearchView.topTitle = lblProducts.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}

//share the business action

-(IBAction)servicesAction:(id)sender
{
    NSLog(@"servicesAction");
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 1  && [[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 1)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"The link to the Download Page has been automatically copied to your Clipboard. Simply “Paste” the link into a message and include your Referrer ID, which is “%@”",[[NSUserDefaults standardUserDefaults] valueForKey:@"id_number"]] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 30;
        [alert show];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You Must Upgrade To Use This Feature." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }

}


-(IBAction)videoAction:(id)sender
{
    NSLog(@"videoAction");
    
    TrainingVideoViewController *objSearchView = (TrainingVideoViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"TrainingVideoViewControllerId"];
    objSearchView.topTitle = lblVideo.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)notificationAction:(id)sender
{
    NSLog(@"notificationAction");

    NotificationCoachViewController *objSearchView = (NotificationCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"NotificationCoachViewControllerId"];
    objSearchView.topTitle = lblNotification.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
   
}


-(IBAction)changePWDAction:(id)sender
{
    NSLog(@"changePWDAction");
    
    ChangePasswordViewController *objSearchView = (ChangePasswordViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ChangePasswordViewControllerId"];
    objSearchView.topTitle = lblChangePwd.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)downLineAction:(id)sender
{
    NSLog(@"downLineAction");
    
    MyDownLineViewController *objSearchView = (MyDownLineViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MyDownLineViewControllerId"];
    objSearchView.topTitle = lblDownline.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}

-(IBAction)upgradeAction:(id)sender
{
    NSLog(@"upgradeAction");
    
    //@"By Upgrading you will be able to send assertive messages and content to your down line of Coaches and to your Customers. You will also be able to have real time Chat communications with your down line. The cost for the Upgraded features are $4.99 / Month. Click the button below to start the payment process."
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"By Upgrading you will be able to send assertive messages and content to your down line of %@ and to your %@. You will also be able to have real time Chat communications with your down line. The cost for the Upgraded features are $%@ / Month. Click the button below to start the payment process.",[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"coach_text"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"customer_text"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"membership_price"]] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Pay Now",@"Cancel", nil];
    alert.tag=10;
    [alert show];
    
}


#pragma mark Pay Pal Integration

//PAYPAL
-(void)payViaPayPal {
    
    NSLog(@"Pay via PayPal");
    
    
    // Remove our last completed payment, just for demo purposes.
    
    self.resultText = nil;
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:@"4.99"];
    
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Payment";
    [payment setIntent:PayPalPaymentIntentAuthorize];
    
    if (!payment.processable)
    {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                   configuration:self.payPalConfig
                                                                        delegate:self];
    // Present the PayPalPaymentViewController.
    [self presentViewController:paymentViewController animated:YES completion:nil];
    
}


- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    //[self showSuccess];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment
{
    
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.description);
    NSDictionary *dictpay4=[completedPayment.confirmation objectForKey:@"response"];
    
    //    NSString *state4=[dictpay4 objectForKey:@"state"];
    //    NSString *idpay4=[dictpay4 objectForKey:@"id"];
    //    NSDictionary *dictpay44=[completedPayment.confirmation objectForKey:@"client"];
    //    NSString *state44=[dictpay44 objectForKey:@"platform"];
    //
    //    NSLog(@"%@\n%@\n%@\n%@\n%@", dictpay4, state4, idpay4, dictpay44, state44);
    
    [self sendAuthorizationToServer:dictpay4];
    
    NSLog(@"dictpay4 %@\n", dictpay4);

    [self SubmitPayPalDetail:dictpay4];
}


- (void)sendAuthorizationToServer:(NSDictionary *)authorization
{
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
    
    
    NSString *clientID = @"Ac6JL86ES5nZfPlGpkmP-3UoK0Qu2_Q_9ZekxwX050MAzRBPBykzERoEwmvGjUcVjjlayOdeWgDx6tCT";
    NSString *secret = @"EFePrWb2pi9Qf5Mwan7yalZLnUCUkXff1le3uWkYXtaOSYJRrgorEEmbq9OPFoXy_DTByIkmBwzfbyVJ";
    
    NSString *authString = [NSString stringWithFormat:@"%@:%@", clientID, secret];
    NSData * authData = [authString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *credentials = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [configuration setHTTPAdditionalHeaders:@{ @"Accept": @"application/json", @"Accept-Language": @"en_US", @"Content-Type": @"application/x-www-form-urlencoded", @"Authorization": credentials }];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    //    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://api.sandbox.paypal.com/v1/oauth2/token"]];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://api.paypal.com/v1/oauth2/token"]];
    request.HTTPMethod = @"POST";
    
    NSString *dataString = @"grant_type=client_credentials";
    NSData *theData = [dataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSURLSessionUploadTask *task = [session uploadTaskWithRequest:request fromData:theData completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSLog(@"data = %@", [NSJSONSerialization JSONObjectWithData:data options:0 error:&error]);
        }
    }];
    
    [task resume];
    
    
}


-(void)shareCoachAction
{
    NSLog(@"shareCoachAction");
    
    
    NSArray *activityItems = [NSArray arrayWithObjects:[NSString stringWithFormat:@"I am so excited to introduce you to who we are and what we do as %@. Just click this link: http://bnotifi.com/super/%@ and watch the short video explaining how this works. When you download the app, use my Referrer ID %@. I'll be in touch to make sure everything works for you, but feel free to contact me with questions.",[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"],[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"],[[NSUserDefaults standardUserDefaults] valueForKey:@"id_number"]], nil];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    [activityVC setValue:@"Super Admin Application" forKey:@"subject"];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         NSLog(@"Activity = %@",activityType);
         NSLog(@"Completed Status = %d",completed);
         
         if (completed)
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Successfully Shared" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
         else
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Unable To Share" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
             
         }
     }];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(IBAction)shareAction:(id)sender
{
    NSLog(@"shareView");
    
    /*
    NSArray *activityItems = [NSArray arrayWithObjects:@"http://bnotifi.com/super/apk/index.php", nil];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    [activityVC setValue:[NSString stringWithFormat:@"Click here to get the %@ Application",[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"]] forKey:@"subject"];

    //[activityVC setValue:@"Click here to get the Super Admin Application" forKey:@"subject"];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         NSLog(@"Activity = %@",activityType);
         NSLog(@"Completed Status = %d",completed);
         
         if (completed)
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Successfully Shared" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
         else
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Unable To Share" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
             
         }
     }];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    */
    
        NSLog(@"servicesAction");
        
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_coach"] integerValue] == 1  && [[[NSUserDefaults standardUserDefaults] valueForKey:@"is_paid"] integerValue] == 1)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"The link to the Download Page has been automatically copied to your Clipboard. Simply “Paste” the link into a message and include your Referrer ID, which is “%@”",[[NSUserDefaults standardUserDefaults] valueForKey:@"id_number"]] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            alert.tag = 30;
            [alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You Must Upgrade To Use This Feature." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        
    
}

//user_id



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
