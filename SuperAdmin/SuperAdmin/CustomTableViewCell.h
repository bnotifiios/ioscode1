//
//  CustomTableViewCell.h
//  NotificationPOC
//
//  Created by Gauri Shankar on 02/04/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property(weak,nonatomic) IBOutlet UILabel *lblTitle;

@end
