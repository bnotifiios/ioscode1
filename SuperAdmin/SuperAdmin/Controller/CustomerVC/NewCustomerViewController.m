//
//  NewCustomerViewController.m
//  SuperAdmin
//
//  Created by Gauri Shankar on 08/11/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import "NewCustomerViewController.h"
#import "MainCoachViewController.h"
#import "AppDelegate.h"
#import "ProductsViewController.h"
#import "ServicesViewController.h"
#import "TrainingVideoViewController.h"
#import "ViewController.h"
#import "ChangePasswordViewController.h"
#import "MyDownLineViewController.h"
#import "LoginViewController.h"
#import "NotificationCoachViewController.h"
#import "Constant.h"
#import "BecomeACoachViewController.h"
#import "WebViewController.h"
#import "ProjectCodeVC.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"

@interface NewCustomerViewController ()<UIAlertViewDelegate>
{
    IBOutlet UIButton *btnBeComeACoach;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    IBOutlet UILabel *lblProduct,*lblVideo,*lblNotification,*lblChangePwd;
    IBOutlet UILabel *lblCount,*lblIAmACoach,*lblBeACoach;

}


@end

@implementation NewCustomerViewController

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)viewWillAppear:(BOOL)animated
{
    [self CallAPI];
    
}

- (void)viewDidLoad
{
    [lblCount setHidden:YES];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];

    //
    lblProduct.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"products_text"];
    lblVideo.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"video_text"];
    lblNotification.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"notification_text"];
    lblChangePwd.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"changepassword_text"];
    lblIAmACoach.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"i_am_coach_text"];
    lblBeACoach.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"be_a_coach_text"];
    
}

#pragma mark API Methods

-(void)CallAPI
{
    NSLog(@"CallAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KBaseActiveNotificationUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
                                            if(data.length > 0)
                                            {
                                                //success
                                                NSLog(@"success");
                                                
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                
                                                NSError *jsonError = nil;
                                                
                                                if (data != nil) {
                                                    
                                                    NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                                    
                                                    if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        if ([[response objectForKey:@"data"]count]>0)
                                                        {
                                                            NSLog(@"success %@",response);
                                                            if([[response objectForKey:@"count"] isEqualToString:@"0"])
                                                            {
                                                                [lblCount setHidden:YES];
                                                            }
                                                            else
                                                            {
                                                                [lblCount setHidden:NO];
                                                                
                                                                lblCount.text = [response objectForKey:@"count"];
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            
                                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                            [alert show];
                                                        }
                                                        
                                                    }
                                                    else
                                                    {
                                                        NSLog(@"failed");
                                                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                        
//                                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                                                        [alert show];
                                                    }
                                                }
                                                
                                            }
                                        }];
    [uploadTask resume];
}

-(IBAction)logOutAction:(id)sender
{
    NSLog(@"loginAction");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
    alert.tag = 20;
    [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 20)
    {
        if(buttonIndex == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_id"];//KUserId
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"status"];//KUserId
            
            ProjectCodeVC *objSearchView = (ProjectCodeVC*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProjectCodeVCId"];
            
            [self.navigationController pushViewController:objSearchView animated:YES];
            objSearchView = nil;
        }
    }
    
    if(alertView.tag == 30)
    {
        if(buttonIndex == 0)
        {
            
            BecomeACoachViewController *objSearchView = (BecomeACoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"BecomeACoachViewControllerId"];
            objSearchView.topTitle = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"i_am_coach_text"];
            
            [self.navigationController pushViewController:objSearchView animated:YES];
            objSearchView = nil;
        }
    }
    
}

-(IBAction)productAction:(id)sender
{
    NSLog(@"productAction");
    
    ProductsViewController *objSearchView = (ProductsViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProductsViewControllerId"];
    objSearchView.topTitle = lblProduct.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)servicesAction:(id)sender
{
    NSLog(@"servicesAction");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://bnotifi.com/super/rest/get_becoach/"]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [[NSUserDefaults standardUserDefaults] valueForKey:@"project_id"]
                       ] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
                                            if(data.length > 0)
                                            {
                                                //success
                                                NSLog(@"success");
                                                
                                                NSError *jsonError = nil;
                                                
                                                if (data != nil) {
                                                    
                                                    NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                                    
                                                    if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        if ([[response objectForKey:@"data"]count]>0)
                                                        {
                                                            NSLog(@"success %@",response);
                                                            
                                                            NSDictionary *dictData = [[response objectForKey:@"data"] objectAtIndex:0];
                                                            
                                                            if ([[dictData valueForKey:@"media_type"] isEqualToString:@"interactive"])
                                                            {
                                                                
                                                                                                                               
                                                                DetailViewController *objRegisterView = (DetailViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"DetailViewControllerId"];
                                                                
                                                        objRegisterView.dictData = dictData;
                                                                [self.navigationController pushViewController:objRegisterView animated:YES];
                                                                objRegisterView = nil;
                                                                
                                                            }
                                                            else
                                                            {
                                                                NSString *strUrl= [NSString stringWithFormat:@"%@/%@",[response objectForKey:@"datasource"],[dictData valueForKey:@"media"]];
                                                                
                                                        
                                                                
                                                                WebViewController *objVC = (WebViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
                                                                
                                                                objVC.strUrl = strUrl;
                                                                objVC.strTitle = @"BE A COACH";
                                                                
                                                                [self.navigationController pushViewController:objVC animated:YES];
                                                                objVC = nil;
                                                            }
                                                            
                                                            
                                                            
                                                        }
                                                        else
                                                        {
                                                            //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            
                                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                            [alert show];
                                                        }
                                                        
                                                    }
                                                    else
                                                    {
                                                        NSLog(@"failed");
                                                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                        
                                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                        [alert show];
                                                    }
                                                }
                                                
                                            }
                                        }];
    
    [uploadTask resume];
}


-(IBAction)videoAction:(id)sender
{
    NSLog(@"videoAction");
    
    TrainingVideoViewController *objSearchView = (TrainingVideoViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"TrainingVideoViewControllerId"];
    objSearchView.topTitle = lblVideo.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)notificationAction:(id)sender
{
    NSLog(@"notificationAction");
    
    NotificationCoachViewController *objSearchView = (NotificationCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"NotificationCoachViewControllerId"];
    objSearchView.topTitle = lblNotification.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)changePWDAction:(id)sender
{
    NSLog(@"changePWDAction");
    
    ChangePasswordViewController *objSearchView = (ChangePasswordViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ChangePasswordViewControllerId"];
    objSearchView.topTitle = lblChangePwd.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)becomeACoachAction:(id)sender
{
    NSLog(@"becomeACoachAction");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"If you already are a %@ then please enter your %@ ID.  By doing this you have access to incredible training videos and \"Walk Out\" content from %@.  You will also then be able to Upgrade your account so that you can Share this opportunity with your friends.",[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"coach_text"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"coach_text"],[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"]] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:@"Enter %@ ID Now",[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"coach_text"]],@"Cancel", nil];
    alert.tag = 30;
    [alert show];
    
}


-(IBAction)shareAction:(id)sender
{
    NSLog(@"shareView");
    
    //
    NSArray *activityItems = [NSArray arrayWithObjects:@"http://bnotifi.com/super/apk/index.php", nil];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
   
    //[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"]
    
    [activityVC setValue:[NSString stringWithFormat:@"Click here to get the %@ Application",[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"]] forKey:@"subject"];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         NSLog(@"Activity = %@",activityType);
         NSLog(@"Completed Status = %d",completed);
         
         if (completed)
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Successfully Shared" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
         else
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Unable To Share" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
             
         }
     }];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
