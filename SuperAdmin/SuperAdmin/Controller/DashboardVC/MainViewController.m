//
//  MainViewController.m
//  SuperAdmin
//
//  Created by Gauri Shankar on 03/11/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "ProductsViewController.h"
#import "ServicesViewController.h"
#import "TrainingVideoViewController.h"
#import "ViewController.h"
#import "ChangePasswordViewController.h"
#import "MyDownLineViewController.h"
#import "LoginViewController.h"
#import "NotificationCoachViewController.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "MusicViewController.h"
#import "ProjectCodeVC.h"


@interface MainViewController ()<UIAlertViewDelegate>
{
    IBOutlet UILabel *lblCount,*lblProduct,*lblMusic,*lblVideo,*lblNotification,*lblChangePwd;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    
    IBOutlet UIView *viewProduct,*viewMusic;
}



@end

@implementation MainViewController


- (void)viewWillAppear:(BOOL)animated
{
    [self CallAPI];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"product_music_type"] isEqualToString:@"0"])
    {
        //Show music tab
        [viewProduct setHidden:YES];
        [viewMusic setHidden:NO];
    }
    else
    {
        //product tab
        [viewProduct setHidden:NO];
        [viewMusic setHidden:YES];
        
    }
}

- (void)viewDidLoad
{
    [lblCount setHidden:YES];
    
    [imgViewAppLogo
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl, [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"project_logo"]]]
     placeholderImage:[UIImage imageNamed:@"app-logo.png"]];
    
    [imgViewBg
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KBaseUrl,[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_image"] ]]
     placeholderImage:[UIImage imageNamed:@""]];
    
    [self.view setBackgroundColor:[AppDelegate colorFromHexString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"background_color"]]];
    
    lblProduct.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"products_text"];
    lblMusic.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"music_text"];
    lblVideo.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"video_text"];
    lblNotification.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"notification_text"];
    lblChangePwd.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"project_detail"] valueForKey:@"changepassword_text"];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    
    [self CallAPI];
    
}

#pragma mark API Methods

-(void)CallAPI
{
    NSLog(@"CallAPI");
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KAcitiveNotifiUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = NSLocalizedString(@"Processing....", nil);
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
                                            if(data.length > 0)
                                            {
                                                //success
                                                NSLog(@"success");
                                                
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                
                                                NSError *jsonError = nil;
                                                
                                                if (data != nil) {
                                                    
                                                    NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                                    
                                                    if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        if ([[response objectForKey:@"data"]count]>0)
                                                        {
                                                            NSLog(@"success %@",response);
                                                            if([[response objectForKey:@"count"] isEqualToString:@"0"])
                                                            {
                                                                [lblCount setHidden:YES];
                                                            }
                                                            else
                                                            {
                                                                [lblCount setHidden:NO];
                                                                
                                                                lblCount.text = [response objectForKey:@"count"];
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            
                                                            //                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                            //                         [alert show];
                                                        }
                                                        
                                                    }
                                                    else
                                                    {
                                                        NSLog(@"failed");
                                                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                        
                                                        //                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                        //                     [alert show];
                                                    }
                                                }
                                                
                                            }
                                        }];
    
    [uploadTask resume];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(IBAction)logOutAction:(id)sender
{
    NSLog(@"loginAction");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
    alert.tag = 20;
    [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 20)
    {
        if(buttonIndex == 0)
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_id"];//KUserId
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"status"];//KUserId
            
            ProjectCodeVC *objSearchView = (ProjectCodeVC*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProjectCodeVCId"];
                        
            [self.navigationController pushViewController:objSearchView animated:YES];
            objSearchView = nil;
        }
    }
    
    
}

#pragma mark Custom methods

-(IBAction)musicAction:(id)sender
{
    NSLog(@"musicAction");
    
    MusicViewController *objSearchView = (MusicViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"MusicViewControllerId"];
    objSearchView.topTitle = lblMusic.text;
    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}

-(IBAction)productAction:(id)sender
{
    NSLog(@"music-productAction");
    
    ProductsViewController *objSearchView = (ProductsViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ProductsViewControllerId"];
    objSearchView.topTitle = lblProduct.text;
    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)videoAction:(id)sender
{
    NSLog(@"videoAction");
    
    TrainingVideoViewController *objSearchView = (TrainingVideoViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"TrainingVideoViewControllerId"];
    objSearchView.topTitle = lblVideo.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)notificationAction:(id)sender
{
    NSLog(@"notificationAction");
    
    NotificationCoachViewController *objSearchView = (NotificationCoachViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"NotificationCoachViewControllerId"];
    objSearchView.topTitle = lblNotification.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)changePWDAction:(id)sender
{
    NSLog(@"changePWDAction");
    
    ChangePasswordViewController *objSearchView = (ChangePasswordViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"ChangePasswordViewControllerId"];
    objSearchView.topTitle = lblChangePwd.text;

    [self.navigationController pushViewController:objSearchView animated:YES];
    objSearchView = nil;
    
}


-(IBAction)shareAction:(id)sender
{
    NSLog(@"shareView");
    
    //
    NSArray *activityItems = [NSArray arrayWithObjects:@"http://bnotifi.com/super/apk/index.php", nil];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    [activityVC setValue:[NSString stringWithFormat:@"Click here to get the %@ Application",[[NSUserDefaults standardUserDefaults] valueForKey:@"Application_Key"]] forKey:@"subject"];

//    [activityVC setValue:@"Click here to get the Super Admin Application" forKey:@"subject"];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         NSLog(@"Activity = %@",activityType);
         NSLog(@"Completed Status = %d",completed);
         
         if (completed)
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Successfully Shared" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
         else
         {
             
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:Nil message:@"Unable To Share" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
             
         }
     }];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
