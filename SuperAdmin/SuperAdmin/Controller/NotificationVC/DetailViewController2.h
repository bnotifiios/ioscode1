//
//  DetailViewController2.h
//  SuperAdmin
//
//  Created by Gauri Shankar on 28/12/16.
//  Copyright © 2016 gauri shankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController2 : UIViewController
{
    NSDictionary *dictData,*dictDataMedia;
}

@property (strong, nonatomic)    NSDictionary *dictData,*dictDataMedia;

@end
