//
//  DetailViewController.m
//  NotificationPOC
//
//  Created by RnF-12 on 15/03/16.
//  Copyright © 2016 RnF-12. All rights reserved.
//

#import "DetailViewController2.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "JSONHelper.h"
#import "Constant.h"
#import "UtilityMethods.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "WebViewController.h"
#import "AppDelegate.h"
#import "PlayViewController.h"

@interface DetailViewController2 ()
{
    NSString *strVideoUrl;
    IBOutlet UIImageView *imgView;
    IBOutlet UIWebView *webViewDoc;
    NSMutableArray *arrInteractiveData;
    // IBOutlet UIButton *btnBack;
    IBOutlet UIImageView *imgViewAppLogo,*imgViewBg;
    int intPlayerHeight,intPlayerHeight1,intPlayerWidth;

}
@end

@implementation DetailViewController2

@synthesize dictData,dictDataMedia;

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button addTarget:self
//               action:@selector(backAction:)
//     forControlEvents:UIControlEventTouchUpInside];
//    [button setBackgroundImage:[UIImage imageNamed:@"back-icon.png"] forState:UIControlStateNormal];
//    [button setBackgroundColor:[UIColor blackColor]];
//    
//    button.frame = CGRectMake(8.0, 22.0, 33.0, 34.0);
//    
//    [self.view addSubview:button];
    
}

- (void)viewDidLoad
{
    intPlayerHeight = 0;
    intPlayerWidth = 0;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"dictData %@",dictData);
    
    arrInteractiveData = [[NSMutableArray alloc] init];

    [self getInteractiveData];
    
}

#pragma mark API Data

-(void)getInteractiveData
{
    
    NSLog(@"getInteractiveData");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:KInteractiveNewButtonUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    //user_id, user_media_id
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"media_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[dictData valueForKey:@"notification_media_id"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * uploadTask =[defaultSession dataTaskWithRequest:request
                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                        
                                        {
                                            
                                            if(data.length > 0)
                                            {
                                                //success
                                                NSLog(@"success");
                                                
                                                NSError *jsonError = nil;
                                                
                                                if (data != nil) {
                                                    
                                                    NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                                    NSLog(@"success %@",response);
                                                    
                                                    if ([[response objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        if ([[response objectForKey:@"data"]count]>0)
                                                        {
                                                            dictDataMedia = [[response objectForKey:@"data"] objectAtIndex:0];
                                                            
                                                            strVideoUrl = [response objectForKey:@"datasource"];
                                                            
                                                            if([[[dictDataMedia valueForKey:@"media"] pathExtension] isEqualToString:@"flv"])
                                                            {
                                                                
                                                                NSString *strUrl= [NSString stringWithFormat:@"%@/%@/mobile/%@",KBaseMediaUrl,[dictDataMedia valueForKey:@"media_type"],[dictDataMedia valueForKey:@"media"]];
                                                                
                                                                NSString *theFileName = [strUrl stringByDeletingPathExtension];
                                                                
                                                                theFileName = [NSString stringWithFormat:@"%@.mp4",theFileName];
                                                                
                                                                [self VideoPlayButtonAction:theFileName];
                                                            }
                                                            else
                                                            {
                                                                [self VideoPlayButtonAction:[NSString stringWithFormat:@"%@/%@/%@",KBaseMediaUrl,[dictDataMedia valueForKey:@"media_type"],[dictDataMedia valueForKey:@"media"]]];
                                                            }
                                                            
                                        
                                                            [arrInteractiveData removeAllObjects];
                                                            
                                                            arrInteractiveData = [response objectForKey:@"buttons"];
                                                            
                                                            if([arrInteractiveData count]>0)
                                                            {
                                                                //check video height & width
                                                                
                                                                if([[dictDataMedia valueForKey:@"media_width"] integerValue] > [[dictData valueForKey:@"media_height"] integerValue])
                                                                {
                                                                    for(int i = 0; i<[arrInteractiveData count];i++)
                                                                    {
                                                                        NSDictionary *dictTemp = [arrInteractiveData objectAtIndex:i];
                                                                        
                                                                        UIButton *but= [UIButton buttonWithType:UIButtonTypeRoundedRect];
                                                                        
                                                                        [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                                                                        but.tag = i;
                                                                        
                                                                        
                                                                        float xVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"position_x"] floatValue];
                                                                        
                                                                        float yVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"position_y"] floatValue]+56;
                                                                        
                                                                        float wVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"width"] floatValue];
                                                                        
                                                                        float hVal = (([[UIScreen mainScreen] bounds].size.width)/800.0)*[[dictTemp valueForKey:@"height"] floatValue];
                                                                        
                                                                        [but setFrame:CGRectMake(xVal, yVal, wVal, hVal)];
                                                                        
                                                                        [but setTitle:[dictTemp valueForKey:@"button_label"] forState:UIControlStateNormal];
                                                                        [but setExclusiveTouch:YES];
                                                                        
                                                                        if([[dictTemp valueForKey:@"button_image"] length] >0)
                                                                        {
                                                                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@button_image/%@",strVideoUrl,[dictTemp valueForKey:@"button_image"]]];
                                                                            NSData *data = [NSData dataWithContentsOfURL:url];
                                                                            UIImage *image = [UIImage imageWithData:data];
                                                                            
                                                                            [but setImage:image forState:UIControlStateNormal];
                                                                        }
                                                                        
                                                                        [but setBackgroundColor:[self colorFromHexString:[dictTemp valueForKey:@"button_color"]]];
                                                                        
                                                                        [self.view addSubview:but];
                                                                        
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    for(int i = 0; i<[arrInteractiveData count];i++)
                                                                    {
                                                                        NSDictionary *dictTemp = [arrInteractiveData objectAtIndex:i];
                                                                        
                                                                        UIButton *but= [UIButton buttonWithType:UIButtonTypeRoundedRect];
                                                                        
                                                                        [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                                                                        but.tag = i;
                                                                        
                                                                        
                                                                      
                                                                        
                                                                        float scale_factor  = ([[UIScreen mainScreen] bounds].size.width)/(600 * ([[dictDataMedia valueForKey:@"media_width"] floatValue]/[[dictDataMedia valueForKey:@"media_height"] floatValue]));
                                                                        
                                                                        int xVal = scale_factor * [[dictTemp valueForKey:@"position_x"] floatValue];
                                                                        
                                                                        int yVal = scale_factor * [[dictTemp valueForKey:@"position_y"] floatValue] + 56;
                                                                        
                                                                        int wVal = scale_factor*[[dictTemp valueForKey:@"width"] floatValue];
                                                                        
                                                                        int hVal = scale_factor*[[dictTemp valueForKey:@"height"] floatValue];
                                                                    
                                                                        
                                                                        [but setFrame:CGRectMake(xVal, yVal, wVal, hVal)];
                                                                        
                                                                        [but setTitle:[dictTemp valueForKey:@"button_label"] forState:UIControlStateNormal];
                                                                        [but setExclusiveTouch:YES];
                                                                        
                                                                        if([[dictTemp valueForKey:@"button_image"] length] >0)
                                                                        {
                                                                            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://bnotifi.com/super/media/media_type/interactive/button_image/%@",[dictTemp valueForKey:@"button_image"]]];
                                                                            NSData *data = [NSData dataWithContentsOfURL:url];
                                                                            UIImage *image = [UIImage imageWithData:data];
                                                                            
                                                                            [but setBackgroundImage:image forState:UIControlStateNormal];
                                                                            
                                                                        }
                                                                        
                                                                        [but setBackgroundColor:[self colorFromHexString:[dictTemp valueForKey:@"button_color"]]];
                                                                        
                                                                        [self.view addSubview:but];
                                                                        
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        }
                                                        
                                                    }
                                                    else
                                                    {
                                                        NSLog(@"failed");
                                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[response objectForKey:@"resultText"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                        [alert show];
                                                    }
                                                }
                                                
                                            }
                                        }];
    [uploadTask resume];
    
    
}

- (UIColor *)colorFromHexString:(NSString *)hexString
{
    NSString *cString = [[hexString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void) buttonClicked:(UIButton*)sender
{
    for (UIView *i in self.view.subviews)
    {
        if(i.tag == 11){
            
            NSLog(@"playerItemDidReachEnd");
            
            [i removeFromSuperview];
            break;
            //i = nil;
        }
    }
    
    NSDictionary *dictTemp = [arrInteractiveData objectAtIndex:sender.tag];
    
    NSLog(@"you clicked on button %ld -- %@", (long)sender.tag,dictTemp);
    
    if([[dictTemp valueForKey:@"button_type"] isEqualToString:@"2"])
    {
        
        if([[[dictTemp valueForKey:@"button_media"] pathExtension] isEqualToString:@"flv"])
        {
            
            NSString *strUrl= [NSString stringWithFormat:@"%@mobile/%@",strVideoUrl,[dictTemp valueForKey:@"button_media"]];
            
            NSString *theFileName = [strUrl stringByDeletingPathExtension];
            
            theFileName = [NSString stringWithFormat:@"%@.mp4",theFileName];
            
            PlayViewController *objVC = (PlayViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"PlayViewControllerId"];
            objVC.strUrl = theFileName;
            
            objVC.strTitle = [dictTemp valueForKey:@"button_label"];
            
            [self.navigationController pushViewController:objVC animated:YES];
            objVC = nil;
            
        }
        else
        {
            PlayViewController *objVC = (PlayViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"PlayViewControllerId"];
            objVC.strUrl = [NSString stringWithFormat:@"%@%@",strVideoUrl,[dictTemp valueForKey:@"button_media"]];
            objVC.strTitle = [dictTemp valueForKey:@"button_label"];
            
            [self.navigationController pushViewController:objVC animated:YES];
            objVC = nil;
            
        }
    }
    else if([[dictTemp valueForKey:@"button_type"] isEqualToString:@"7"])//survey
    {
        
        WebViewController *objVC = (WebViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
        objVC.strUrl = [NSString stringWithFormat:@"%@/%@",[dictTemp valueForKey:@"button_media"],[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]];
        objVC.strTitle = [dictTemp valueForKey:@"button_label"];
        objVC.strMediaType = @"notification";
        objVC.dictData = dictDataMedia;
        
        [self.navigationController pushViewController:objVC animated:YES];
        objVC = nil;
        
    }
    else if([[dictTemp valueForKey:@"button_type"] isEqualToString:@"8"])//inteactive
    {
        dictData = dictTemp;

        [self getInteractiveData];

    }
    else
    {
        WebViewController *objVC = (WebViewController*)[[AppDelegate storyBoardType] instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
        objVC.strUrl = [NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"button_media"]];
        objVC.strTitle = [dictTemp valueForKey:@"button_label"];
        objVC.strMediaType = @"notification";
        objVC.dictData = dictDataMedia;
        
        [self.navigationController pushViewController:objVC animated:YES];
        objVC = nil;
        
    }
    
}

#pragma mark Video Play

-(void)VideoPlayButtonAction:(NSString*)strUrl
{
    NSLog(@"VideoPlayButtonAction");
    
    if ([strUrl length]>0)
    {
        NSString *urlString =
        [NSString stringWithFormat:@"%@",
         [strUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        
        // asset is a video
        AVPlayer *avPlayer = [[AVPlayer alloc] initWithURL:[NSURL URLWithString:urlString]];
        //avPlayer.muted = YES;
        
        NSLog(@"Video url %@",urlString);
        
        AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avPlayer];
        UIView *subView;
        
        [subView setBackgroundColor:[UIColor redColor]];
        [avPlayerLayer setBackgroundColor:(__bridge CGColorRef _Nullable)([UIColor greenColor])];
        
        if ([[dictDataMedia valueForKey:@"media_type"] isEqualToString:@"interactive"])
        {
            
            if([[dictDataMedia valueForKey:@"media_width"] intValue]>[[dictDataMedia valueForKey:@"media_height"] intValue])
            {
                
                intPlayerWidth = [[UIScreen mainScreen] bounds].size.width;
                
                intPlayerHeight = ([[dictDataMedia valueForKey:@"media_height"] intValue] * intPlayerWidth ) / [[dictDataMedia valueForKey:@"media_width"] intValue];
                
            }
            else
            {
                intPlayerHeight = [[UIScreen mainScreen] bounds].size.height;
                
                intPlayerWidth = ([[dictDataMedia valueForKey:@"media_width"] intValue] * intPlayerHeight) / [[dictDataMedia valueForKey:@"media_height"] intValue];
            }
            
            
            [avPlayerLayer setFrame:CGRectMake(0, 0, intPlayerWidth, intPlayerHeight)];
            subView = [[UIView alloc]initWithFrame:CGRectMake(0, 56, intPlayerWidth, intPlayerHeight)];
           
        }
        else
        {
            if(IS_IPHONE_5)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 320, 568)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
            }
            else if(IS_IPHONE_6)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 375, 667)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 375, 667)];
                
            }
            else if(IS_IPHONE_6P)
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 414, 768)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 414, 768)];
                
            }
            else
            {
                [avPlayerLayer setFrame:CGRectMake(0, 56, 320, 480)];
                subView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
                
            }
            //avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        }
        
        //avPlayerLayer.anchorPoint = CGPointMake(0.5, 0);
        subView.tag = 11;
        [subView.layer addSublayer:avPlayerLayer];
        
        [self.view addSubview:subView];
        
        [avPlayer play];
        
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[avPlayer currentItem]];
        
    }
    else
    {
        NSLog(@"Video not available");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video not available" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    
    for (UIView *i in self.view.subviews)
    {
        if(i.tag == 11){
            
            NSLog(@"playerItemDidReachEnd");
            
            //[i removeFromSuperview];
            break;
            //i = nil;
        }
    }
}

@end
